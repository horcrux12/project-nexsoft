-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Mar 2021 pada 12.08
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nd6sp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `idProduct` varchar(100) NOT NULL,
  `productName` varchar(250) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`idProduct`, `productName`, `qty`, `price`) VALUES
('PRD0001', 'Nestlé Koko Krunch 330 gr', 432, 39900),
('PRD0002', 'Milo', 441, 30000),
('PRD0003', 'Dancow', 430, 45000),
('PRD0004', 'Lactogen Lactogrow 750gr', 435, 95000),
('PRD0005', 'Nescafe Cappucino 500gr', 451, 48500),
('PRD0006', 'Nestle pure life 330ml 24pcs', 487, 37000),
('PRD0007', 'Nestle Batita 900gr', 488, 68000),
('PRD0008', ' Nestlé Kitkat Gold 8X17Gr 2F', 100, 44900),
('PRD0009', 'Nestlé Susu Bear Brand 189 Ml 1 Dus Isi 30 Pcs', 100, 280000),
('PRD0010', 'Gery Saluut Malkist Sweet Cheese -110g - 5pcs', 150, 35000),
('PRD0011', 'Gery Dark Chocolatos Mini Wafer Roll 1 Karton', 150, 120900);

-- --------------------------------------------------------

--
-- Struktur dari tabel `promotion`
--

CREATE TABLE `promotion` (
  `idPromotion` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `forPurchase` double NOT NULL,
  `method` enum('Discount','Free Good') NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `promotion`
--

INSERT INTO `promotion` (`idPromotion`, `description`, `startDate`, `endDate`, `forPurchase`, `method`, `status`) VALUES
('PROMO0001', 'tset Description', '2021-03-18', '2021-03-19', 500000, 'Free Good', 0),
('PROMO0002', 'Disc. 20% Milo dan Koko Crunch', '2021-03-10', '2021-03-31', 100000, 'Discount', 1),
('PROMO0003', 'Dancow Disc. 15%', '2021-03-10', '2021-04-30', 50000, 'Discount', 1),
('PROMO0007', 'Buy Milo Rp.1.500.000 Disc. 20%', '2021-03-10', '2021-03-31', 1500000, 'Discount', 1),
('PROMO0008', 'tset Description2', '2021-03-18', '2021-03-19', 500000, 'Free Good', 1),
('PROMO0009', 'Test Promotion1', '2021-03-11', '2021-03-26', 150000, 'Discount', 1),
('PROMO0010', 'Buy Milo Rp. 60000 Disc 20%', '2021-03-15', '2021-03-17', 60000, 'Discount', 1),
('PROMO0011', 'Buy Nestle Batita & Nescafe Rp.120.000 discount Rp. 30.000', '2021-03-17', '2021-03-31', 120000, 'Discount', 1),
('PROMO0012', 'Buy Nescafe Rp.100000 Get 1 Nestle Purelife', '2021-03-18', '2021-03-31', 100000, 'Free Good', 1),
('PROMO0013', 'Buy Nestle Purelife Rp. 70000 Disc. Rp. 15000', '2021-03-25', '2021-03-31', 70000, 'Discount', 1),
('PROMO0014', 'Buy Susu Bear Brand Rp. 500.000 Disc. 15%', '2021-03-24', '2021-03-27', 500000, 'Discount', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction`
--

CREATE TABLE `transaction` (
  `idTransaction` varchar(100) NOT NULL,
  `buyer` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `netAmount` double NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaction`
--

INSERT INTO `transaction` (`idTransaction`, `buyer`, `description`, `netAmount`, `created_date`) VALUES
('TRANS0001', 'Silo ', 'Test Transaction', 2553030, '2021-03-22'),
('TRANS0002', 'Budi', 'Budi buy Nestle Purelife', 207000, '2021-03-23'),
('TRANS0003', 'Dadang', 'Dadang buy some package', 1481850, '2021-03-24'),
('TRANS0004', 'Andika', 'Andika buy some package', 614610, '2021-03-24'),
('TRANS0005', 'Bambang', 'Bambang buy Garry Dark Chocolatos', 1005800, '2021-03-24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_items`
--

CREATE TABLE `trans_items` (
  `idTransItem` varchar(100) NOT NULL,
  `idTransaction` varchar(100) NOT NULL,
  `idProduct` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trans_items`
--

INSERT INTO `trans_items` (`idTransItem`, `idTransaction`, `idProduct`, `price`, `qty`) VALUES
('202ceb09-b732-4ce1-9285-fdf572c77c7f', 'TRANS0001', 'PRD0003', 45000, 9),
('2537909b-11b9-46d5-86bb-7cea9ff3c5ed', 'TRANS0001', 'PRD0005', 48500, 7),
('2e0b4da6-ebef-4b89-865d-b0e2f92e7feb', 'TRANS0001', 'PRD0001', 39900, 9),
('40b9b3a5-b52a-453b-bac2-63d0c288cccb', 'TRANS0001', 'PRD0007', 68000, 6),
('5ef5d745-bd70-4955-8296-8f6a7fd60fef', 'TRANS0004', 'PRD0001', 39900, 8),
('60ef92bd-253d-485c-b5bb-907d4abd3a84', 'TRANS0004', 'PRD0003', 45000, 5),
('74169b87-2212-4857-96c0-b2d144fd25c2', 'TRANS0004', 'PRD0002', 30000, 7),
('833d8b6c-31b5-42f3-9267-239575450a6d', 'TRANS0002', 'PRD0006', 37000, 6),
('89c4a749-8cb8-47c4-aa72-024a340323e9', 'TRANS0001', 'PRD0004', 95000, 8),
('8cd1fd4a-4b6a-4f02-8806-d76016ba30b3', 'TRANS0001', 'PRD0002', 30000, 8),
('a108dbec-8cc6-4f2d-8dce-e39deac46a9a', 'TRANS0005', 'PRD0003', 45000, 6),
('a36f8ece-2cd6-4d24-880b-0e3ca7419f2e', 'TRANS0001', 'PRD0006', 37000, 6),
('c5f07fc6-a665-4ddd-beb1-f4377c6aff22', 'TRANS0005', 'PRD0011', 110900, 7),
('d5611322-2e6f-45b7-a0f8-697de4b8d956', 'TRANS0003', 'PRD0004', 95000, 9),
('d94db54b-f1e4-48c8-9558-9f2d298dba0f', 'TRANS0003', 'PRD0003', 45000, 7),
('de40aba6-9cbb-41b1-ad38-a8306af3fd10', 'TRANS0003', 'PRD0001', 39900, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_promotion`
--

CREATE TABLE `trans_promotion` (
  `idTransPromotion` varchar(100) NOT NULL,
  `idTransaction` varchar(100) NOT NULL,
  `idPromotion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trans_promotion`
--

INSERT INTO `trans_promotion` (`idTransPromotion`, `idTransaction`, `idPromotion`) VALUES
('296e5368-d1a5-400d-ab75-505a8e8d72cf', 'TRANS0004', 'PROMO0002'),
('39fdb15c-d6fa-47df-9dc4-5f06925d13d0', 'TRANS0001', 'PROMO0002'),
('68acba41-5433-4ca8-84b1-04019144cd8c', 'TRANS0003', 'PROMO0003'),
('6da8caa5-36dc-4516-8c9c-1587140fdfd9', 'TRANS0001', 'PROMO0003'),
('7c5d0aa8-566f-4b23-810f-d87d8aca8270', 'TRANS0002', 'PROMO0013'),
('bd42fc2d-c2dc-412e-b48e-695ed1985f27', 'TRANS0001', 'PROMO0012'),
('e20e0d4c-966e-4735-b16d-c33ca4da7786', 'TRANS0004', 'PROMO0003'),
('eee1d6b5-33c5-48d4-905f-fcbdea0ac794', 'TRANS0005', 'PROMO0003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `idUser` varchar(100) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`idUser`, `username`, `password`, `name`) VALUES
('USR0001', 'Admins', '$2a$10$DWeZm1pviy5tOHLopUxDMOiIUs1CvPuLzBPQBGP223PoJK8FZkEca', 'silo'),
('USR0002', 'Admins1', '$2a$10$MVVkC1zb3JwCHj8CS5SjReuLa5r5h2PyxbA..NPILYmZj50LBe2am', 'Sales');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wcb`
--

CREATE TABLE `wcb` (
  `idWcb` varchar(100) NOT NULL,
  `idPromotion` varchar(100) NOT NULL,
  `idProduct` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `wcb`
--

INSERT INTO `wcb` (`idWcb`, `idPromotion`, `idProduct`) VALUES
('054b53f2-7707-4ba4-bbd5-f0de6cfb6fab', 'PROMO0008', 'PRD0001'),
('079a6acb-e9fb-4f8c-a1ef-31a5890affd7', 'PROMO0011', 'PRD0007'),
('19e1d7e3-02b2-4e44-bbf9-ea002d6bd4bf', 'PROMO0001', 'PRD0002'),
('1d7d383a-ff57-4b94-965d-8b7d9bf8e503', 'PROMO0002', 'PRD0001'),
('1f2847b2-0995-43ca-9fb1-72bdf2d2f49a', 'PROMO0003', 'PRD0003'),
('35b6689e-5954-4a4d-8085-c1d0aa1b077c', 'PROMO0012', 'PRD0005'),
('41274892-7579-44ef-8df0-ba1c293bd166', 'PROMO0010', 'PRD0002'),
('718877ed-74e6-43f9-a578-a6e04d87996e', 'PROMO0002', 'PRD0002'),
('7b14625b-fd1f-4482-8868-7b0bdc329350', 'PROMO0008', 'PRD0002'),
('997b4da8-fbf0-406b-bba5-0d65757c7f48', 'PROMO0007', 'PRD0002'),
('a735fd80-b4bb-406b-8886-71673869becb', 'PROMO0014', 'PRD0009'),
('b22fbeee-b03e-45e1-beef-b44b3e994766', 'PROMO0013', 'PRD0006'),
('b8d577c9-7187-401c-af63-7b0b9ff4d414', 'PROMO0009', 'PRD0002'),
('c438d793-5163-4788-9246-5f7d6a9a8da7', 'PROMO0009', 'PRD0004'),
('cd6e9d22-9621-4fda-abaa-3b1dfb3bac0a', 'PROMO0001', 'PRD0001'),
('eec82171-b0fe-4cb2-b3ec-5045e95e8e05', 'PROMO0011', 'PRD0005');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wcg`
--

CREATE TABLE `wcg` (
  `idWcg` varchar(100) NOT NULL,
  `idPromotion` varchar(100) NOT NULL,
  `idProduct` varchar(100) DEFAULT NULL,
  `qtyWcg` int(11) DEFAULT NULL,
  `discount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `wcg`
--

INSERT INTO `wcg` (`idWcg`, `idPromotion`, `idProduct`, `qtyWcg`, `discount`) VALUES
('10054ca2-fe76-434f-bb03-8b49bf394c1d', 'PROMO0010', NULL, NULL, 18),
('1d71b1ac-4a50-4d42-a76a-f3d9cd527064', 'PROMO0012', 'PRD0006', 1, NULL),
('721da215-1f52-4ddd-b218-7fc047385c91', 'PROMO0011', NULL, NULL, 30000),
('7ad51333-8aa2-48cc-88fe-afb957533ff4', 'PROMO0007', NULL, NULL, 20),
('8942a6b3-81bf-4b85-a3c9-557090c3d694', 'PROMO0009', NULL, NULL, 20),
('8cff405f-a163-49c2-9fdd-f667bd6a5b79', 'PROMO0008', 'PRD0003', 3, NULL),
('c8ecbf7d-d83e-4249-b99d-8674ac82d2b7', 'PROMO0002', NULL, NULL, 20),
('c9c716f0-95d7-4f5a-a2e1-96e4c0c491c1', 'PROMO0001', 'PRD0003', 3, NULL),
('ed78fa63-b8d3-495e-afd1-1bcd5fe1f467', 'PROMO0013', NULL, NULL, 15000),
('f02c4222-753e-4101-8acf-d12afb44bc7a', 'PROMO0014', NULL, NULL, 15),
('f3734f49-9d21-4f48-a1be-d35ab8a3013b', 'PROMO0003', NULL, NULL, 15);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`idProduct`);

--
-- Indeks untuk tabel `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`idPromotion`);

--
-- Indeks untuk tabel `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`idTransaction`);

--
-- Indeks untuk tabel `trans_items`
--
ALTER TABLE `trans_items`
  ADD PRIMARY KEY (`idTransItem`),
  ADD KEY `idTransaction` (`idTransaction`),
  ADD KEY `idProduct` (`idProduct`);

--
-- Indeks untuk tabel `trans_promotion`
--
ALTER TABLE `trans_promotion`
  ADD PRIMARY KEY (`idTransPromotion`),
  ADD KEY `idTransaction` (`idTransaction`),
  ADD KEY `idPromotion` (`idPromotion`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- Indeks untuk tabel `wcb`
--
ALTER TABLE `wcb`
  ADD PRIMARY KEY (`idWcb`),
  ADD KEY `idPromotion` (`idPromotion`),
  ADD KEY `idProduct` (`idProduct`);

--
-- Indeks untuk tabel `wcg`
--
ALTER TABLE `wcg`
  ADD PRIMARY KEY (`idWcg`),
  ADD KEY `idPromotion` (`idPromotion`),
  ADD KEY `idProduct` (`idProduct`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `trans_items`
--
ALTER TABLE `trans_items`
  ADD CONSTRAINT `trans_items_ibfk_1` FOREIGN KEY (`idProduct`) REFERENCES `product` (`idProduct`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trans_items_ibfk_2` FOREIGN KEY (`idTransaction`) REFERENCES `transaction` (`idTransaction`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `trans_promotion`
--
ALTER TABLE `trans_promotion`
  ADD CONSTRAINT `trans_promotion_ibfk_1` FOREIGN KEY (`idTransaction`) REFERENCES `transaction` (`idTransaction`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trans_promotion_ibfk_2` FOREIGN KEY (`idPromotion`) REFERENCES `promotion` (`idPromotion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `wcb`
--
ALTER TABLE `wcb`
  ADD CONSTRAINT `wcb_ibfk_1` FOREIGN KEY (`idProduct`) REFERENCES `product` (`idProduct`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wcb_ibfk_2` FOREIGN KEY (`idPromotion`) REFERENCES `promotion` (`idPromotion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `wcg`
--
ALTER TABLE `wcg`
  ADD CONSTRAINT `wcg_ibfk_1` FOREIGN KEY (`idProduct`) REFERENCES `product` (`idProduct`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wcg_ibfk_2` FOREIGN KEY (`idPromotion`) REFERENCES `promotion` (`idPromotion`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
