const loginDefault = {
    validate : false
};

//Reducer
const recaptReducer = (state = loginDefault, action) => {
   switch (action.type) {
        case 'VALID_RECAPT' : 
           return{
               ...state,
               validate : true
           }
        default:
           return state;
   }
}

export default recaptReducer;