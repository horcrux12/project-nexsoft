import { combineReducers } from "redux";
import AuthReducer from "./auth";
import RecaptReducer from "./recapt";

const reducers = combineReducers({
    AuthReducer : AuthReducer,
    RecaptReducer : RecaptReducer, 
})

export default reducers