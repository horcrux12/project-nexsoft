import jwt_decode from "jwt-decode";
// import moment from "moment";

const loginDefault = {
    statusLogin : false,
    tokenRecapt : null,
    dataLogin : null,
    token : null,
};

//Reducer
const authReducer = (state = loginDefault, action) => {
   switch (action.type) {
         case 'LOGIN_SUCCCESS':
            return{
                  ...state,
                  statusLogin : true,
                  dataLogin : jwt_decode(action.payload),
                  token : action.payload
            }
         case 'LOGOUT' :
            return loginDefault;
         case 'SET_RECAPT_TOKEN' : 
            return{
                  ...state,
                  tokenRecapt : action.payload
            }
         default:
           return state;
   }
}

export default authReducer;