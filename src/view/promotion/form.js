import React, { Component } from 'react';
import { Autocomplete } from "@material-ui/lab";
import DateFnsUtils from '@date-io/date-fns';
import { withStyles } from "@material-ui/core/styles";
import DeleteIcon from '@material-ui/icons/Delete';
import { red } from "@material-ui/core/colors"
import { Grid, Typography, FormControl,
    TextField, Paper, Tabs, Tab, Input, 
    InputAdornment, FormHelperText,
    ThemeProvider, createMuiTheme, 
    TableContainer, Table, TableHead,
    TableRow, TableBody } 
    from "@material-ui/core";
import { KeyboardDatePicker,
    MuiPickersUtilsProvider  } 
    from "@material-ui/pickers";
import {connect} from "react-redux";

import { StyledTableCell, StyledTableRow } from "../../components/table";
import Action from "../../action";
import { ButtonKuning, ButtonHijau, 
    ButtonMerah} from '../../components/button';
import priceConvert from "../../util/priceConvert";
import formatDate from "../../util/formatDate";

const styles = theme => ({
    root : {
        padding : theme.spacing(4)
    },
    judul : {
        display : "inline-block",
        backgroundColor : "#c70039",
        padding : "5px 50px 5px 15px",
        borderTopRightRadius: "70px",
        borderBottomLeftRadius: "22px",
        fontWeight : "bold",
        color : "#fff5ea"
    },
    fieldForm : {
        marginBottom : "10px"
    },
    mt5 : {
        marginTop : "25px"
    },
    mt2 :{
        margin: "0.4vh"
    },
    table: {
        minWidth: 650,
        "& .MuiTableCell-root": {
          borderLeft: "1px solid rgba(224, 224, 224, 1)"
        }
    }
});


const custTheme = createMuiTheme({
    overrides: {
        MuiPickersToolbar: {
          toolbar: {
            backgroundColor: "#c70039",
          },
        },
        MuiPickersDay: {
          day: {
            color: "#000",
          },
          daySelected: {
            backgroundColor: "#c70039",
          },
          dayDisabled: {
            color: red["100"],
          },
          current: {
            color: red["900"],
          },
        },
        MuiPickersModal: {
          dialogAction: {
            color: red["400"],
          },
        },
    },
});

class FormPromotion extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            startDate : {value : new Date(), valid : true},
            toggleEndDate : true,
            toggleTabs : 0,
            options : [],
            endDate : {value : null, valid : true},
            description : {value : "", valid : true},
            discount : {value : 0, valid : true},
            forPurchase : {value : 0, valid : true},
            rowsWcb : [],
            rowsWcbValid : true,
            totalWcb : 0,
            wcbSelect : null,
            fgSelect : null,
            fgQty : {value : 0, valid : true},
            rowsFg : [],
            rowsFgValid : true,
            dataUpdate : {}
        }
        this.handleDateChange = this.handleDateChange.bind(this);
        this.toggleTabs = this.toggleTabs.bind(this);
        this.tabPanel = this.tabPanel.bind(this);
        this.setHeaderValue = this.setHeaderValue.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.addWcbHandle = this.addWcbHandle.bind(this);
        this.addFgHandle = this.addFgHandle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        const {description, startDate,
            endDate, forPurchase, rowsWcb,
            totalWcb, toggleTabs, rowsFg,
            discount } = this.state;

        if (Object.keys(this.props.match.params).length > 0) {
            // console.log(this.props.match.params.id);
            fetch(`http://localhost:8080/api/sales/promotion/${this.props.match.params.id}`,
                {
                    method : "GET",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization" : "Bearer " + this.props.token
                    }
                })
            .then(resp => {
                if (!resp.ok) {
                    if (resp.status === 403){
                        return resp.json().then(text => {
                            alert(text.message);
                            this.props.logout();
                            this.props.history.push("/login");
                        })
                    }
                    return resp.json().then(text => {
                        throw new Error(`${text.message}`);
                    })
                }
                return resp.json();
            })
            .then(json => {
                console.log(json);
                if (json.method === "Discount") {
                    this.setState({
                        toggleTabs : 0,
                        description : {...description, value:json.promotionDesc},
                        startDate : {...startDate, value : new Date(json.startDate)},
                        endDate : {...endDate, value : new Date(json.endDate)},
                        toggleEndDate : false,
                        forPurchase : {...forPurchase, value : json.forPurchase},
                        rowsWcb : json.whatCustBuy,
                        discount : {...discount, value : json.whatCustGet[0].discount},
                        totalWcb : json.whatCustBuy.reduce((a,b) => a + b, 0),
                    })
                }else{
                    // let fg = ;
                    // let newArr = 
                    this.setState({
                        toggleTabs : 1,
                        description : {...description, value:json.promotionDesc},
                        startDate : {...startDate, value : new Date(json.startDate)},
                        endDate : {...endDate, value : new Date(json.endDate)},
                        toggleEndDate : false,
                        forPurchase : {...forPurchase, value : json.forPurchase},
                        rowsWcb : json.whatCustBuy,
                        rowsFg : json.whatCustGet.map((el, indx) => {
                            return {
                                ...el.freeGood,
                                qty : el.qty
                            }
                        }),
                        totalWcb : json.whatCustBuy.reduce((a,b) => a + b, 0)
                    })
                }
            })
            .catch(err => {
                alert(err);
            })
        }
        fetch(`http://localhost:8080/api/sales/product`,
            {
                method : "GET",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization" : "Bearer " + this.props.token
                }
            })
        .then(resp => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    throw new Error(`${text.message}`);
                })
            }
            return resp.json();
        })
        .then(json => {
            this.setState({
                options : json.data
            })
        })
        .catch(err => {
            alert(err);
        })
    }
    
    // Handle
    toggleTabs(e, value){  
        this.setState({
            toggleTabs : value
        });
    }
    
    handleDateChange(value, key) {
        let validation = true;

        validation = value !== null ? true : false
        if (key === "startDate") {
            this.setState({
                [key] : {
                    value : value,
                    valid : validation
                },
                toggleEndDate : !(value !== null ? value.toString() !== "Invalid Date" ? true : false : false),
                endDate : {
                    ...this.state.endDate,
                    valid : true
                }
            });
        }else{
            this.setState({
                [key] : {
                    value : value,
                    valid : validation
                },
            });
        }
    }
    
    handleChange(el, key){
        let value = el.target.value;
        let validate = true;
        if (key === "forPurchase" || key === "discount") {
            
            if (value === "")
                validate = false
            if (value < this.state.totalWcb && key === "forPurchase")
                validate = false
            if ((value >= 80 ? ((this.state.totalWcb*0.79) < value) : value > 0 ? false : true) && key === "discount")
                validate = false

            this.setState({
                [key] : {
                    value : parseInt(value),
                    valid : validate
                },
            });
        }else if(key === "fgQty"){
            let limitQty = Math.floor(this.state.fgSelect.qty*0.30)
            if (value < 0)
                value = 0
            if (value >= limitQty)
                value = limitQty

            this.setState({
                [key] : {
                    ...this.state.fgQty,
                    value : parseInt(value),
                },
            });
        }else{
            this.setState({
                [key] : parseInt(value),
            });
        }
    }

    setHeaderValue(el, key){
        let value = el.target.value;
        let validation = true
        if(value.trim().length < 1){
            validation = false
        }
        this.setState({
            [key] : {
                value : value,
                valid : validation
            }
        })
    }

    addWcbHandle(){
        const {wcbSelect, rowsWcb,
            forPurchase, discount} = this.state;
        let newArray = rowsWcb;
        let findExistingProduct = rowsWcb.findIndex(val => val.idProduct===wcbSelect.idProduct);
        if (wcbSelect === null) {
            alert("Select the product first!!");
        }else if (findExistingProduct >= 0){
            alert(`Product ${wcbSelect.productName} has already selected`);
        }else {
            newArray.push(wcbSelect);
            let arrPrice = newArray.map(el => el.price);
            let total = arrPrice.reduce((a,b) => a + b, 0);
            // console.log(discount.value > 80 ? ((total - (total*0.1)) > discount.value) : true)
            this.setState({
                rowsWcb : newArray,
                rowsWcbValid : newArray.length > 0 ? true : false,
                wcbSelect : null,
                totalWcb : total,
                forPurchase : {
                    value : forPurchase.value, 
                    valid : total < forPurchase.value
                },
                discount : {
                    ...discount,
                    valid : discount.value > 80 ? ((total - (total*0.1)) > discount.value) : true
                }
            })
        }
    }

    addFgHandle(){
        const {fgSelect, rowsFg,
            fgQty} = this.state;
        let newArray = rowsFg;
        let findExistingProduct = rowsFg.findIndex(val => val.idProduct===fgSelect.idProduct);
        if (fgSelect === null) {
            alert("Select the product first!!");
        }else if (findExistingProduct >= 0){
            alert(`Product ${fgSelect.productName} has already selected`);
        }else if (fgQty.value < 1){
            alert("make sure your Qty is valid");
        }else {
            let newObj = {
                ...fgSelect,
                qty : fgQty.value
            }
            newArray.push(newObj)
            this.setState({
                rowsFg : newArray,
                rowsFgValid : newArray.length > 0 ? true : false,
                fgSelect : null,
                fgQty : {
                    value : 0,
                    valid : true
                }
            })
        }
    }

    handleSubmit(){
        const {description, startDate,
            endDate, forPurchase, rowsWcb,
            totalWcb, toggleTabs, rowsFg,
            discount } = this.state;
        let dataInput = {};
        let doInput = false;
        // console.log(discount.value && discount.value > (totalWcb - (totalWcb*0.1)));
        if (toggleTabs === 0) {
            if (description.value.toString().trim().length <= 0 ||
                (startDate.value !== null ? startDate.value.toString() !== "Invalid Date" ? false : true : true)||
                (endDate.value !== null ? endDate.value.toString() !== "Invalid Date" ? false : true : true)||
                (forPurchase.value && forPurchase.value <= totalWcb) ||
                (discount.value && discount.value > (totalWcb*0.79)) ||
                rowsWcb.length <= 0
            ){
                this.setState({
                    description : {...description, valid : description.value.trim().length > 0},
                    startDate : {...startDate, valid : (startDate.value !== null ? startDate.value.toString() !== "Invalid Date" ? true : false : false)},
                    endDate : {...endDate, valid : (endDate.value !== null ? endDate.value.toString() !== "Invalid Date" ? true : false : false)},
                    forPurchase : {...forPurchase, valid : (forPurchase.value && forPurchase.value > totalWcb)},
                    rowsWcbValid : rowsWcb.length > 0 ? true : false,
                    discount : {...discount, valid : (discount.value > 80 ? ((totalWcb*0.79) > discount.value) : discount.value > 0 ? true : false)}
                })
                alert("unable to submit, please check the form");
            }else{
                dataInput = {
                    promotionDesc : description.value,
                    startDate : formatDate(startDate.value),
                    endDate : formatDate(endDate.value),
                    forPurchase : forPurchase.value,
                    method : "Discount",
                    whatCustBuy : rowsWcb.map(el => ({idProduct : el.idProduct})),
                    whatCustGet : [{
                        discount : discount.value
                    }]
                }
                doInput = true;
            }
        }else{
            if (description.value.trim().length <= 0 ||
                (startDate.value !== null ? startDate.value.toString() !== "Invalid Date" ? false : true : true)||
                (endDate.value !== null ? endDate.value.toString() !== "Invalid Date" ? false : true : true)||
                (forPurchase.value.toString().trim().length <= 0 && forPurchase.value <= 0) ||
                rowsWcb.length <= 0 || rowsFg.length <= 0
            ){
                this.setState({
                    description : {...description, valid : description.value.trim().length > 0},
                    startDate : {...startDate, valid : (startDate.value !== null ? startDate.value.toString() !== "Invalid Date" ? true : false : false)},
                    endDate : {...endDate, valid : (endDate.value !== null ? endDate.value.toString() !== "Invalid Date" ? true : false : false)},
                    forPurchase : {...forPurchase, valid : (forPurchase.value && forPurchase.value > totalWcb)},
                    rowsWcbValid : rowsWcb.length > 0 ? true : false,
                    rowsFgValid : rowsFg.length > 0 ? true : false,
                })
                alert("unable to submit, please check the form");
            }else{
                dataInput = {
                    promotionDesc : description.value,
                    startDate : formatDate(startDate.value),
                    endDate : formatDate(endDate.value),
                    forPurchase : forPurchase.value,
                    method : "Free Good",
                    whatCustBuy : rowsWcb.map(el => ({idProduct : el.idProduct})),
                    whatCustGet : rowsFg.map(el => ({
                        freeGood : {
                            idProduct : el.idProduct
                        },
                        qty : el.qty
                    }))
                }
                doInput = true;
            }
        }

        if (doInput) {
            fetch(`http://localhost:8080/api/sales/promotion/${Object.keys(this.props.match.params).length > 0 ?
             `update/${this.props.match.params.id}` : "create"}`,
                {
                    method : Object.keys(this.props.match.params).length > 0 ? "PUT" : "POST",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization" : "Bearer " + this.props.token
                    },
                    body : JSON.stringify(dataInput)
                })
            .then(resp => {
                if (!resp.ok) {
                    if (resp.status === 403){
                        return resp.json().then(text => {
                            alert(text.message);
                            this.props.logout();
                            this.props.history.push("/login");
                        })
                    }
                    return resp.json().then(text => {
                        throw new Error(`${text.message}`);
                    })
                }
                return resp.json();
            })
            .then(json => {
                alert(json.message);
                this.props.history.push("/admin/promotion")
            })
            .catch(err => {
                alert(err);
            })
        }
    }
    // End Handle

    tabPanel(index){
        const{ discount, forPurchase,
            totalWcb, fgQty, fgSelect,
            rowsFg, rowsFgValid } = this.state;
        const{classes} = this.props;

        return(
            <div
                role="tabpanel"
                id={`scrollable-force-tabpanel-${index}`}
                aria-labelledby={`scrollable-force-tab-${index}`}
                >
                {index === 0 ? (
                    <Grid container alignItems="center" className={(this.props.classes) && this.props.classes.root} spacing={3}>
                        <Grid item xs={2}>
                            <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Value</Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <FormControl fullWidth>
                                <Input
                                    type="number"
                                    min="0"
                                    value={forPurchase.value}
                                    onChange={(el) => {this.handleChange(el, 'forPurchase')}}
                                    startAdornment={<InputAdornment position="start">Rp</InputAdornment>}
                                />
                                {!forPurchase.valid &&
                                <FormHelperText error>
                                    This field cannot be empty and must be more than {priceConvert(totalWcb)}
                                </FormHelperText>}
                            </FormControl>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Discount</Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <FormControl fullWidth>
                                <Input
                                    error = {!discount.valid}
                                    type="number"
                                    min="0"
                                    value={discount.value}
                                    onChange={(el) => {this.handleChange(el, 'discount')}}
                                    endAdornment={(discount.value <= 79) && <InputAdornment position="end">%</InputAdornment>}
                                    startAdornment={(discount.value >= 80) && <InputAdornment position="start">Rp</InputAdornment>}
                                />
                                { 
                                    !discount.valid &&
                                    <FormHelperText error>
                                        This field cannot be empty and cannot be more than {priceConvert((totalWcb*0.79))}
                                    </FormHelperText>
                                }
                            </FormControl>
                        </Grid>
                    </Grid>
                ) :
                (
                    <Grid container alignItems="center" className={(this.props.classes) && this.props.classes.root} spacing={3}>
                        <Grid item xs={2}>
                            <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Value</Typography>
                        </Grid>
                        <Grid item xs={10}>
                            <FormControl fullWidth>
                                <Input
                                    type="number"
                                    min="0"
                                    value={forPurchase.value}
                                    onChange={(el) => {this.handleChange(el, 'forPurchase')}}
                                    startAdornment={<InputAdornment position="start">Rp</InputAdornment>}
                                />
                                {!forPurchase.valid &&
                                <FormHelperText error>
                                    This field cannot be empty and must be more than {priceConvert(totalWcb)}
                                </FormHelperText>}
                            </FormControl>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Product</Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Autocomplete
                                id="combo-box-demo"
                                value = {fgSelect}
                                onChange={(event, value) => {
                                    this.setState({
                                        fgSelect : value,
                                        fgQty : {
                                            ...fgQty,
                                            valid : value ? false : true
                                        }
                                    })
                                }}
                                // onInputChange={(event, value) => {console.log(value)}}
                                options={this.state.options}
                                getOptionLabel={(option) => { return option.productName}}
                                renderInput={(params) => { return <TextField {...params} label="Select Product" variant="outlined" fullWidth />}}
                            />
                        </Grid>
                        <Grid item xs={1}>
                            <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Qty</Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <FormControl fullWidth>
                                <Input
                                    type="number"
                                    min="0"
                                    value={fgQty.value}
                                    onChange={(el) => {this.handleChange(el, 'fgQty')}}
                                />
                                {!fgQty.valid &&
                                <FormHelperText error>
                                    This field cannot be empty and must be more than 0, and can't be greater than {Math.floor(fgSelect.qty*0.30)}
                                </FormHelperText>}
                            </FormControl>
                        </Grid>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={10}>
                            <ButtonHijau onClick={this.addFgHandle}>
                                Add Product
                            </ButtonHijau>
                        </Grid>
                        <Grid item xs={12} className={classes.mt5}>
                            <TableContainer component={Paper}>
                                <Table className={classes.table} aria-label="customized table">
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell align="center">ID Product</StyledTableCell>
                                            <StyledTableCell align="center">Product</StyledTableCell>
                                            <StyledTableCell align="center">Qty</StyledTableCell>
                                            <StyledTableCell align="center">Action</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {
                                        rowsFg.length > 0 ? 
                                        rowsFg.map((row, indx) => (
                                            <StyledTableRow key={row.idProduct}>
                                                <StyledTableCell component="th" scope="row">
                                                    {row.idProduct}
                                                </StyledTableCell>
                                                <StyledTableCell >{row.productName}</StyledTableCell>
                                                <StyledTableCell >{row.qty}</StyledTableCell>
                                                <StyledTableCell align="center">
                                                    <ButtonMerah size="small" 
                                                            startIcon={<DeleteIcon />} 
                                                            variant="outlined" 
                                                            color="secondary"
                                                            onClick={()=>{
                                                                if(window.confirm("Are you sure you want to delete this record?")){
                                                                    let newArr = rowsFg;
                                                                    newArr.splice(indx, 1);
                                                                    this.setState({
                                                                        rowsFg : newArr,
                                                                        rowsFgValid : newArr.length > 0 ? true : false
                                                                    })
                                                                }
                                                            }}>
                                                        detele
                                                    </ButtonMerah>
                                                </StyledTableCell>
                                            </StyledTableRow>)) : 
                                            
                                            <StyledTableRow>
                                                <StyledTableCell colSpan="4">No product selected</StyledTableCell>
                                            </StyledTableRow>

                                    }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                            {!rowsFgValid &&
                                <FormHelperText error>
                                    Add at least one of product on table
                                </FormHelperText>}
                        </Grid>
                    </Grid>
                )
                }
            </div>
        )
    }

    render() { 
        const { classes, history } = this.props;
        const { startDate, endDate, 
            toggleEndDate, toggleTabs,
            rowsWcb, wcbSelect, discount, 
            description, forPurchase,
            rowsWcbValid } = this.state;

        return (  
            <Grid container className={classes.root}>
                <Grid container spacing={3}>

                    {/* Header Promotion */}
                    <Grid item xs={12}>
                        <Paper elevation={3}>
                            <Typography className={classes.judul} variant="h5">Header Promotion</Typography>
                            <Grid container className={classes.root}>
                                <Grid container className={classes.fieldForm} spacing={2}
                                    alignItems="center"
                                >
                                    <Grid item xs={2}>
                                        <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Description</Typography>
                                    </Grid>
                                    <Grid item xs={5}>
                                        <TextField
                                            error={!description.valid}
                                            value={description.value}
                                            onChange={(el)=>{this.setHeaderValue(el,"description")}}
                                            id="outlined-multiline-static"
                                            multiline
                                            rows={4}
                                            variant="outlined"
                                            fullWidth={true}
                                        />
                                        { 
                                        !description.valid &&
                                            <FormHelperText error>
                                                This field cannot be empty
                                            </FormHelperText>
                                        }
                                    </Grid>
                                    <Grid item xs={5}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={4}>
                                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Start date</Typography>
                                            </Grid>
                                            <Grid item xs={8}>
                                                <ThemeProvider theme={custTheme}>
                                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                        <KeyboardDatePicker
                                                            clearable
                                                            value={startDate.value}
                                                            placeholder="yyyy/MM/dd"
                                                            onChange={date => this.handleDateChange(date,"startDate")}
                                                            minDate={new Date()}
                                                            format="yyyy/MM/dd"
                                                            fullWidth={true}
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </ThemeProvider>
                                                { 
                                                !startDate.valid &&
                                                    <FormHelperText error>
                                                        This field cannot be empty
                                                    </FormHelperText>
                                                }
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>End date</Typography>
                                            </Grid>
                                            <Grid item xs={8}>
                                                <ThemeProvider theme={custTheme}>
                                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                        <KeyboardDatePicker
                                                            disabled = {toggleEndDate}
                                                            clearable
                                                            value={endDate.value}
                                                            placeholder="yyyy/MM/dd"
                                                            onChange={date => this.handleDateChange(date, "endDate")}
                                                            minDate={startDate.value}
                                                            format="yyyy/MM/dd"
                                                            fullWidth={true}
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </ThemeProvider>
                                                { 
                                                !endDate.valid &&
                                                    <FormHelperText error>
                                                        This field cannot be empty
                                                    </FormHelperText>
                                                }
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                
                            </Grid>
                        </Paper>
                    </Grid>

                    {/* What Customer Gets */}
                    <Grid item xs={12}>
                        <Paper elevation={3}>
                            <Typography className={classes.judul} variant="h5">What Customer Get(s)</Typography>
                            <Grid container>
                                <Grid item xs={12}>
                                    <Tabs
                                        value={toggleTabs}
                                        onChange={this.toggleTabs}
                                        indicatorColor="secondary"
                                        textColor="secondary"
                                        centered
                                    >
                                        <Tab label="Discount"/> 
                                        <Tab label="Free Good"/>
                                    </Tabs>
                                </Grid>
                                <Grid item xs={12}>
                                    {this.tabPanel(toggleTabs)}
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>

                    {/* Whats Customer Buys */}
                    <Grid item xs={12}>
                        <Paper elevation={3}>
                            <Typography className={classes.judul} variant="h5">What Customer Buy(s)</Typography>
                            <Grid container className={classes.root} spacing={2} alignItems="center">
                                <Grid item xs={2}>
                                    <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Product</Typography>
                                </Grid>
                                <Grid item xs={8}>
                                    <Autocomplete
                                        id="combo-box-demo"
                                        value = {wcbSelect}
                                        onChange={(event, value) => {this.setState({wcbSelect : value})}}
                                        // onInputChange={(event, value) => {console.log(value)}}
                                        options={this.state.options}
                                        getOptionLabel={(option) => { return option.productName}}
                                        renderInput={(params) => { return <TextField {...params} label="Select Product" variant="outlined" fullWidth />}}
                                    />
                                </Grid>
                                <Grid item xs={2} container justify="flex-end">
                                    <ButtonHijau onClick={this.addWcbHandle}>
                                        Add Product
                                    </ButtonHijau>
                                </Grid>
                                <Grid item xs={12} className={classes.mt5}>
                                    <TableContainer component={Paper}>
                                        <Table className={classes.table} aria-label="customized table">
                                            <TableHead>
                                                <TableRow>
                                                    <StyledTableCell align="center">Product</StyledTableCell>
                                                    <StyledTableCell align="center">Price</StyledTableCell>
                                                    <StyledTableCell align="center">Action</StyledTableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                            {
                                                rowsWcb.length > 0 ? 
                                                rowsWcb.map((row, indx) => (
                                                    <StyledTableRow key={row.idProduct}>
                                                        <StyledTableCell component="th" scope="row">
                                                            {row.productName}
                                                        </StyledTableCell>
                                                        <StyledTableCell align="right">{priceConvert(row.price)}</StyledTableCell>
                                                        <StyledTableCell align="center">
                                                            <ButtonMerah size="small" 
                                                                    startIcon={<DeleteIcon />} 
                                                                    variant="outlined" 
                                                                    color="secondary"
                                                                    onClick={()=>{
                                                                        if(window.confirm("Are you sure you want to delete this record?")){
                                                                            let newArr = rowsWcb;
                                                                            newArr.splice(indx, 1);
                                                                            let arrPrice = newArr.map(el => el.price)
                                                                            let total = arrPrice.reduce((a,b) => a + b, 0)
                                                                            this.setState({
                                                                                rowsWcb : newArr,
                                                                                rowsWcbValid : newArr.length > 0 ? true : false,
                                                                                totalWcb : total,
                                                                                forPurchase : {
                                                                                    value : forPurchase.value, 
                                                                                    valid : total < forPurchase.value
                                                                                },
                                                                                discount : {
                                                                                    ...discount,
                                                                                    valid : discount.value > 80 ? ((total - (total*0.1)) > discount.value) : true
                                                                                }
                                                                            })
                                                                        }
                                                                    }}>
                                                                detele
                                                            </ButtonMerah>
                                                        </StyledTableCell>
                                                    </StyledTableRow>)) : 
                                                    
                                                    <StyledTableRow>
                                                        <StyledTableCell colSpan="3">No product selected</StyledTableCell>
                                                    </StyledTableRow>
                                            }
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    {!rowsWcbValid &&
                                        <FormHelperText error>
                                            Add at least one of product on table
                                        </FormHelperText>}
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    
                    <Grid item container justify="flex-end" xs={12} spacing={3}>
                        <Grid item>
                            <ButtonHijau
                                onClick={this.handleSubmit}
                            >
                                Save
                            </ButtonHijau>
                        </Grid>
                        <Grid item>
                            <ButtonMerah
                                onClick={() => {
                                    if(window.confirm("Are you sure to leave this page ?")){
                                        history.push("/admin/promotion")
                                    }
                                }} 
                            >
                                Cancel
                            </ButtonMerah>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return{
        isLogin : state.AuthReducer.statusLogin,
        validCaptcha : state.RecaptReducer.validate,
        token : state.AuthReducer.token
    }
}

const mapDispatchToProps = dispatch => {
    return{
        doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin}),
        setTokenRecapt : (token) => dispatch({ type : Action.SET_RECAPT_TOKEN, payload : token}),
        validRecapt : () => dispatch({ type : Action.VALID_RECAPT}),
        logout : () => dispatch({ type : Action.LOGOUT})
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, {withTheme : true})(FormPromotion));