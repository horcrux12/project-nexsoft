import React, { Component } from 'react';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import DeleteIcon from "@material-ui/icons/Delete";
import CreateIcon from '@material-ui/icons/Create';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { withStyles } from "@material-ui/core/styles";
import { Grid, Typography, IconButton, Input,
    FormControl, InputLabel, InputAdornment,
    Select, MenuItem, TableContainer, Table,
    TableHead, TableRow, TableBody, Button,
    Tooltip, Backdrop, CircularProgress,
    Dialog, DialogActions, DialogTitle, 
    DialogContent, Snackbar, Paper
} from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import MuiAlert from '@material-ui/lab/Alert';
import {connect} from "react-redux";

import { ButtonHijau, ButtonKuning, 
    ButtonMerah } from "../../components/button";
import { StyledTableCell, StyledTableRow } from "../../components/table"
import Action from "../../action";
import priceConvert from "../../util/priceConvert";

const styles = theme => ({
    root : {
        padding : theme.spacing(4)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    mt2 :{
        margin: "2px"
    },
    table: {
        minWidth: 650,
        "& .MuiTableCell-root": {
          borderLeft: "1px solid rgba(224, 224, 224, 1)"
        }
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    judul : {
        display : "inline-block",
        backgroundColor : "#c70039",
        padding : "5px 50px 5px 15px",
        borderTopRightRadius: "70px",
        borderBottomLeftRadius: "22px",
        fontWeight : "bold",
        color : "#fff5ea"
    }
});

class TablePromotion extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            searchType : "idPromotion",
            keyword : "",
            limit: 5,
            currentPage : 1,
            data : [],
            page : 0,
            openBackdrop : false,
            openDialog : false,
            openSnackBar : false,
            dataDetail : {}
        }
        this.handleChange = this.handleChange.bind(this);
        this.searchHandle = this.searchHandle.bind(this);
        this.searchHandleBtn = this.searchHandleBtn.bind(this);
        this.handlePage = this.handlePage.bind(this);
        this.getData = this.getData.bind(this);
        this.deleteFunc = this.deleteFunc.bind(this);
        this.closeSnackBar = this.closeSnackBar.bind(this);
        this.openSnackBar = this.openSnackBar.bind(this);
    }

    componentDidMount(){
        const { limit } = this.state;
        fetch(`http://localhost:8080/api/sales/promotion?limit=${limit}&offset=0`,
        {
            method : "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization" : "Bearer " + this.props.token
            }
        })
        .then(resp => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    // console.log(text);
                    throw new Error(`${text.message}`);
                })
            }
            return resp.json();
        })
        .then(json => {
            this.setState({
                data : json.data,
                page : Math.ceil(json.size/this.state.limit)
            })
        })
        .catch(err => {
            alert(err);
        })
    }

    handleChange(el, key){

        if(key === "searchType"){
            this.setState({
                [key] : el.target.value,
                keyword : ""
            }, () => {
                this.getData();
            })
        }else{
            this.setState({
                [key] : el.target.value
            }, () => {
                this.getData();
            })
        }
    }

    searchHandle(el){
        if(el.keyCode === 13){
            this.setState({
                keyword : el.target.value
            }, () => {
                this.getData();
            })
        }
    }

    handlePage(event, value){
        this.setState({
            currentPage : value
        }, () => {
            this.getData();
        })
    }

    searchHandleBtn(){
        this.getData();
    }

    getData(){
        const { limit, page, currentPage,
            searchType, keyword, 
            openBackdrop } = this.state;
        let query = "";
        let arrQuery = [];
        arrQuery.push(`limit=${limit}`);
        arrQuery.push(`offset=${(limit*currentPage) - limit}`);

        keyword.length > 0 && 
            searchType === "idPromotion" && arrQuery.push(`idPromotion=${keyword}`);
            searchType === "productName" && arrQuery.push(`productName=${keyword}`);
        query = arrQuery.join("&");
        
        this.setState({
            openBackdrop : !openBackdrop
        },()=>{
            fetch(`http://localhost:8080/api/sales/promotion?${query}`,
            {
                method : "GET",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization" : "Bearer " + this.props.token
                }
            })
            .then(resp => {
                if (!resp.ok) {
                    if (resp.status === 403){
                        return resp.json().then(text => {
                            alert(text.message);
                            this.props.logout();
                            this.props.history.push("/login");
                        })
                    }
                    return resp.json().then(text => {
                        throw new Error(`${text.message}`);
                    })
                }
                return resp.json();
            })
            .then(json => {
                this.setState({
                    data : json.data,
                    page : Math.ceil(json.size/this.state.limit),
                    openBackdrop : !this.state.openBackdrop
                })
            })
            .catch(err => {
                alert(err);
            })
        })
    }

    deleteFunc(idPromotion, indx){
        const{ data } = this.state;
        if (window.confirm("Are you sure to delete this data ?")) {
            fetch(`http://localhost:8080/api/sales/promotion/delete/${idPromotion}`, {
                method : "DELETE",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization" : "Bearer " + this.props.token
                }
            })
            .then(resp => {
                if (!resp.ok) {
                    if (resp.status === 403){
                        return resp.json().then(text => {
                            alert(text.message);
                            this.props.logout();
                            this.props.history.push("/login");
                        })
                    }
                    return resp.json().then(text => {
                        throw new Error(`${text.message}`);
                    })
                }
                return resp.json();
            })
            .then(json => {
                alert(json.message);
                this.setState({
                    currentPage : 1,
                    keyword : ""
                },()=>{
                    this.getData();
                })
            })
            .catch(err => {
                alert(err);
            })
        }
    }

    closeSnackBar(event, reason){
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            openSnackBar : false
        })
    }

    openSnackBar(){
        this.setState({
            openSnackBar : true
        })
    }

    render() { 
        const {classes, history} = this.props;
        const {searchType, keyword,
            limit, currentPage,
            page, data, openBackdrop,
            openDialog, dataDetail,
            openSnackBar} = this.state;
        
        let dataTable = "";
        let dataDialog = "";
        if (data.length > 0) {
            dataTable = data.map((promotion, indx) => {
                return(
                    <StyledTableRow key={promotion.idPromotion}>
                        <StyledTableCell >{promotion.idPromotion}</StyledTableCell>
                        <StyledTableCell >{promotion.promotionDesc}</StyledTableCell>
                        <StyledTableCell >{promotion.startDate}</StyledTableCell>
                        <StyledTableCell >{promotion.endDate}</StyledTableCell>
                        <StyledTableCell align="center">
                            <Tooltip title="Detail" arrow>
                                <Button 
                                    className={classes.mt2} size="small" 
                                    startIcon={<ErrorOutlineIcon />} variant="contained" 
                                    color="primary"
                                    onClick={() => {
                                        this.setState({
                                            openDialog : true,
                                            dataDetail : data[indx]
                                        }, () => {
                                            console.log(this.state.dataDetail)
                                        })
                                    }}>detail
                                </Button>
                            </Tooltip>
                            <Tooltip title="Edit" arrow>
                                <ButtonKuning 
                                className={classes.mt2} size="small" 
                                startIcon={<CreateIcon />} variant="contained"
                                onClick={() => {
                                    history.push("/admin/edit-promotion/"+promotion.idPromotion);
                                }} 
                                >Edit
                            </ButtonKuning>
                            </Tooltip>
                            <Tooltip title="Delete" arrow>
                                <ButtonMerah 
                                    className={classes.mt2} size="small" 
                                    startIcon={<DeleteIcon />} variant="contained"
                                    onClick={() => {this.deleteFunc(promotion.idPromotion, indx)}}>
                                    Delete
                                </ButtonMerah>
                            </Tooltip>
                        </StyledTableCell>
                    </StyledTableRow>
                )
            })
        }else{
            dataTable =
            <StyledTableRow>
                <StyledTableCell colSpan={5}>No data available</StyledTableCell>
            </StyledTableRow>;
        } 

        if (Object.keys(dataDetail).length > 0) {
            dataDialog = <Dialog
                fullWidth={true}
                maxWidth="md"
                scroll="paper"
                open={openDialog}
                onClose={() => {
                    this.setState({
                        openDialog : false,
                        dataDetail : {}
                    });
                }}
                aria-labelledby="max-width-dialog-title"
            >
                <DialogTitle id="max-width-dialog-title">Detail Promotion</DialogTitle>
                    <DialogContent style={{height:"515px"}}>
                        {/* <DialogContentText>
                            You can set my maximum width and whether to adapt or not.
                        </DialogContentText> */}
                        <Grid container>
                            <Grid item container spacing={3}>
                                <Grid item xs={12}>
                                    <Paper elevation={3}>

                                        <Typography className={classes.judul} variant="subtitle1">Header Promotion</Typography>
                                        <Grid container className={classes.root}>
                                            <Grid container spacing={2}>
                                                <Grid item xs={3}>
                                                    <Typography variant="subtitle2" >Description</Typography>
                                                </Grid>
                                                <Grid item xs={9} container alignItems="flex-start" spacing={2}>
                                                    <Grid item>
                                                        <Typography variant="subtitle2">:</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography variant="subtitle2">{dataDetail.promotionDesc}</Typography>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <Typography variant="subtitle2" >Promotion Duration</Typography>
                                                </Grid>
                                                <Grid item xs={9} container alignItems="flex-start" spacing={2}>
                                                    <Grid item>
                                                        <Typography variant="subtitle2">:</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography variant="subtitle2">{`${dataDetail.startDate} to ${dataDetail.endDate}`}</Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12}>
                                    <Paper elevation={3}>

                                        <Typography className={classes.judul} variant="subtitle1">What Customer Buy(s)</Typography>
                                        <Grid container className={classes.root}>
                                            <Grid container spacing={2}>
                                                <Grid item xs={3}>
                                                    <Typography variant="subtitle2" >For puchase</Typography>
                                                </Grid>
                                                <Grid item xs={9} container alignItems="flex-start" spacing={2}>
                                                    <Grid item>
                                                        <Typography variant="subtitle2">:</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography variant="subtitle2">{priceConvert(dataDetail.forPurchase)}</Typography>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <Typography variant="subtitle2" >Items to buys</Typography>
                                                </Grid>
                                                <Grid item xs={9} container alignItems="flex-start" spacing={2}>
                                                    <Grid item>
                                                        <Typography variant="subtitle2">:</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        {dataDetail.whatCustBuy.map((el, indx) => 
                                                            (<Typography key={indx} variant="subtitle2">
                                                                {`${el.productName}`}
                                                            </Typography>)
                                                        )}
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12}>
                                    <Paper elevation={3}>
                                        <Typography className={classes.judul} variant="subtitle1">What Customer Get(s)</Typography>
                                        <Grid container className={classes.root}>
                                            <Grid container spacing={2}>
                                                <Grid item xs={3}>
                                                    <Typography variant="subtitle2" >
                                                        {dataDetail.method === "Discount" ? "Discount" : "Item will get(s)"}
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={9} container alignItems="flex-start" spacing={2}>
                                                    <Grid item>
                                                        <Typography variant="subtitle2">:</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        {dataDetail.method === "Discount" ? 
                                                            <Typography variant="subtitle2">{dataDetail.whatCustGet[0].discount < 80 ? 
                                                                `${dataDetail.whatCustGet[0].discount}%` : `${priceConvert(dataDetail.whatCustGet[0].discount)}`}
                                                            </Typography> :
                                                            dataDetail.whatCustGet.map((el,indx) => (
                                                                <Typography key={indx} variant="subtitle2">
                                                                    {`${el.freeGood.productName} @${el.qty} pcs`}
                                                                </Typography>
                                                            ))
                                                        }
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                            </Grid>
                        </Grid>
                    </DialogContent>
                <DialogActions>
                    <Button onClick={() => {
                            this.setState({
                                openDialog : false,
                                dataDetail : {}
                            });
                        }} color="primary">
                        Close
                    </Button>
                </DialogActions>
            </Dialog>;
        }

        return (
            <Grid container className={classes.root}>
                <Backdrop className={classes.backdrop} open={openBackdrop}>
                    <CircularProgress color="inherit" />
                </Backdrop>
                <Grid container spacing={3} alignItems="center">
                    <Grid item xs={12} style={{textAlign: "center"}}>
                        <Typography variant="h3" style={{fontWeight: "500"}}> Promotion Table </Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <FormControl fullWidth={true}>
                            <InputLabel htmlFor="age-native-simple">Search By</InputLabel>
                            <Select
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            value={searchType}
                            onChange={(el) => {this.handleChange(el, "searchType")}}
                            inputProps={{
                                name: 'age',
                                id: 'age-native-simple',
                            }}
                            >
                                <MenuItem value={"idPromotion"}>Promotion Code</MenuItem>
                                <MenuItem value={"productName"}>Product Name</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={7}>
                        <FormControl fullWidth={true}>
                            <InputLabel>Input keyword</InputLabel>
                            <Input
                                type="text"
                                onChange={(e)=> {
                                    this.setState({
                                        keyword : e.target.value
                                    },() => {
                                        e.target.value.length === 0 &&
                                            this.getData()
                                    })
                                }}
                                onKeyUp={this.searchHandle}
                                value={keyword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton onClick={this.searchHandleBtn}>
                                            <SearchIcon/>
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                    <Grid container item xs={2} justify="flex-end">
                        <ButtonHijau startIcon={<AddIcon />} onClick={() => {history.push("/admin/add-promotion")}}>
                            Add Promotion
                        </ButtonHijau>
                    </Grid>
                    <Grid item xs={12}>
                        <TableContainer>
                            <Table aria-label="customized table" className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell align="center">Promotion Code</StyledTableCell>
                                        <StyledTableCell align="center">Description</StyledTableCell>
                                        <StyledTableCell align="center">Start Date</StyledTableCell>
                                        <StyledTableCell align="center">End Date</StyledTableCell>
                                        <StyledTableCell align="center">Action</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {dataTable}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                    <Grid item container xs={12} justify="center">
                        <Grid item container xs={2} spacing={3} alignItems="center">
                            <Grid item>
                                <Typography >Show : </Typography>
                            </Grid>
                            <Grid item>
                                <FormControl>
                                    <Select
                                        value={limit}
                                        onChange={(el) => {this.handleChange(el, "limit")}}
                                        inputProps={{
                                            name: 'limit',
                                        }}
                                    >
                                        <MenuItem value={5}>5</MenuItem>
                                        <MenuItem value={10}>10</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid item container xs={10} justify="flex-end">
                            {
                                page > 1 && 
                                <Pagination count={page} page={currentPage} onChange={this.handlePage}/>
                            }
                        </Grid>
                    </Grid>
                </Grid>
                {dataDialog}
                <Snackbar open={openSnackBar} autoHideDuration={6000} onClose={this.closeSnackBar}>
                    <Alert onClose={this.closeSnackBar} severity="success">
                        Success deleted data
                    </Alert>
                </Snackbar>
            </Grid>  
        );
    }
}

const mapStateToProps = state => {
    return{
        isLogin : state.AuthReducer.statusLogin,
        validCaptcha : state.RecaptReducer.validate,
        token : state.AuthReducer.token
    }
}

const Alert = props => <MuiAlert elevation={6} variant="filled" {...props} />

const mapDispatchToProps = dispatch => {
    return{
        doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin}),
        setTokenRecapt : (token) => dispatch({ type : Action.SET_RECAPT_TOKEN, payload : token}),
        validRecapt : () => dispatch({ type : Action.VALID_RECAPT}),
        logout : () => dispatch({ type : Action.LOGOUT})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, {withTheme:true})(TablePromotion));