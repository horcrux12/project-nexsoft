import React, { Component } from 'react';
import {withStyles, createMuiTheme, ThemeProvider} from "@material-ui/core/styles"
import { grey } from "@material-ui/core/colors";
import { FormControl, InputAdornment, Button,
    InputLabel, IconButton, Input,
    FormHelperText
    } from "@material-ui/core";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import logoNd6 from "../../assets/image/nd6antelope.png";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import ReCAPTCHA from "react-google-recaptcha";

import Action from "../../action";

const styles = theme => {return ({
    formControl: {
        margin: theme.spacing.unit,
        marginBottom : "12px",
    },
    formCenter: {
        margin: theme.spacing.unit,
        marginBottom : "12px",
        alignItems : "center"
    },
    root: {
        width: "100%",
        minHeight : "100vh",
        backgroundColor: "#4e3d53",
        color: "#222831",
        fontSize: "1rem",
        fontStyle: "normal",
        fontWeight: 400,
        fontFamily: "Arial, Helvetica, sans-serif",
        display: "flex",
        justifyContent : "center",
        alignItems : "center"
    },
    container : {
        width: "500px",
        overflow: "hidden",
        padding: "55px 55px 37px",
        background : "#1e212d",
        boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
    },
    formLogin : {
        margin : 0,
        color : "#848391"
    },
    header : {
        height: "20%",
        display: "flex",
        flexDirection : "column"
    },
    judulLogin : {
        fontSize: "30px",
        fontWeight : 600,
        color: "#848391",
        lineHeight: "1.2",
        textAlign: "center",
        textTransform: "uppercase",
        webkitTextStroke : "1px white"
    },
    descLogin : {
        textAlign : "center"
    },
    logo : {
        display : "flex",
        width : "250px",
        justifyContent : "center",
        alignItems: "center",
        margin: "3px auto 5px auto",
    },
    form : {
        width: "80%",
        margin: "2vh auto 5vh auto",
        display: "flex",
        flexDirection: "column",
        color : "#afaebd"
    },
    multilineColor :{
        color : grey[600],
        borderBottom: "2px solid #848391",
        '&:hover': {
            borderBottom: "2px solid #848391",
        }
    },
    labelColor : {
        color : "#848391",
        '&:hover': {
            borderBottom: "2px solid #848391",
        }
    },
    fabProgress : {
        color : grey[600]
    },
    underline: {
		color: theme.palette.common.white,
		borderBottom: theme.palette.common.white,
		'&:after': {
			borderBottom: `2px solid ${theme.palette.common.white}`,			
		},				
		'&:focused::after': {
			borderBottom: `2px solid ${theme.palette.common.white}`,
		},		
		'&:before': {
			borderBottom: `1px solid ${theme.palette.common.white}`,			
		},
		'&:hover:not($disabled):not($focused):not($error):before': {
			borderBottom: '2px solid rgb(255, 255, 255) !important',
		},
		'&$disabled:before': {
			borderBottom: `1px dotted ${theme.palette.common.white}`,
		},
	},
})};

const customTheme = createMuiTheme({
    palette : {
        type :"dark"
    }
})

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recaptchaInstance : null, 
            username : {
                value : "",
                validate : false
            },
            password : {
                value : "",
                validate : false
            },
            showPassword : false,
            recaptcha : false,
            isValid : false,
            isUpdate : true,
        };
        this.key = 0;
        this.handleChange = this.handleChange.bind(this);
        this.handleClickShowPassword = this.handleClickShowPassword.bind(this);
        this.handleMouseDownPassword = this.handleMouseDownPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
        this.expiredRecaptcha = this.expiredRecaptcha.bind(this);
        this.onVerify = this.onVerify.bind(this);
    }

    componentDidMount(){
        if (this.captchaDemo) {
            console.log("started, just a second...")
            this.captchaDemo.reset();
        }
    }

    handleChange (event, prop){
        const pattUname = new RegExp("^(?=.*[a-z])(?=.*[A-Z]).{6,12}$");
        const pattPass = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[?!@#$%^&+=0-9]).{8,}$")
        let validate = false
        if (prop === "username") {
            if (!pattUname.test(event.target.value)){
                validate = true;
            }
        }

        if (prop === "password") {
            if (!pattPass.test(event.target.value)){
                validate = true;
            }
        }
        this.setState({ 
            [prop]: {
                value : event.target.value,
                validate : validate,
                isUpdate : true
            } 
        });
    };
    
    handleClickShowPassword () {
        this.setState({ showPassword: !this.state.showPassword });
    };
    
    handleMouseDownPassword (event)  {
        event.preventDefault();
    };

    onLoadRecaptcha(){
        if (this.captchaDemo) {
            this.captchaDemo.reset();
        }
        this.setState({
            recaptcha : !this.state.recaptcha
        })
    }

    expiredRecaptcha(el){
        // console.log("onerror")
        this.captchaDemo.reset();
    }

    onVerify(resp){
        // console.log(resp)
        this.setState({
            isValid : !this.state.isValid,
        },()=>{
            this.props.setRecaptToken(resp);
            this.props.validRecapt();
        })
    }

    handleSubmit () {
        // console.log(this.state.isValid)
        if (!this.state.isValid) {
            alert("ReCaptcha tidak valid!!")
        }else{
            fetch(`http://localhost:8080/api/sales/authtoken?username=${this.state.username.value}&password=${this.state.password.value}`,
            {
                method : "POST",
                headers: {"Content-type": "application/json; charset=UTF-8"}
            })
            .then(resp => {
                // console.log(resp)
                if (!resp.ok) {
                    return resp.json().then(text => {
                        throw new Error(`${text.message}`);
                    })
                }
                return resp.json();
            })
            .then(json => {
                // console.log(json);
                this.props.doLogin(json.token);
            })
            .catch( status => {
                // console.log(status);
                alert(status);
            })
        }
    }

    render() { 
        const { classes, isLogin } = this.props;
        const { username, password, 
            showPassword, recaptcha } = this.state;
        if (isLogin) {
            return (<Redirect to="/"/>)
        }
        // console.log("renderred")
        return (  
            <>
                <div className={classes.root}>
                    <div className={classes.container}>
                        <div className={classes.formLogin}>
                            <div className={classes.header}>
                                <img className={classes.logo} src={logoNd6} alt="Logo ND6"/>
                                <div className={classes.judulLogin}>
                                    Login
                                </div>
                                {/* <Typography variant="h3" align="alignCenter" color="inherit" gutterBottom>Login</Typography> */}
                                <span className={classes.descLogin}>
                                    Masukkan username dan Password pada form dibawah ini 
                                </span>
                            </div>
                            <div className={classes.form}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel className={classes.labelColor} htmlFor="standard-adornment-password">Username</InputLabel>
                                        <Input
                                            error = {username.validate}
                                            type="text"
                                            value={username.value}
                                            onChange={(e) => {this.handleChange(e,'username')}}
                                            classes = {{
                                                underline: classes.underline
                                            }}
                                        />
                                        
                                    </FormControl>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel className={classes.labelColor} htmlFor="standard-adornment-password">Password</InputLabel>
                                        <Input
                                            error={password.validate}
                                            id="standard-adornment-password"
                                            type={showPassword ? 'text' : 'password'}
                                            value={password.value}
                                            onChange={(e) => {this.handleChange(e,'password')}}
                                            classes = {{
                                                underline: classes.underline
                                            }}
                                            endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={this.handleClickShowPassword}
                                                onMouseDown={this.handleMouseDownPassword}
                                                >
                                                {showPassword ? <Visibility style={{color:"white"}}/> : <VisibilityOff style={{color:"white"}}/>}
                                                </IconButton>
                                            </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                    <FormControl className={classes.FormControl} fullWidth style={{textAlign:"-webkit-center"}}>
                                        <ReCAPTCHA
                                            ref={(el) => {this.captchaDemo = el;}}
                                            fullWidth={true}
                                            size="normal"
                                            data-theme="dark"            
                                            render="explicit"
                                            sitekey="6LfvS3EaAAAAAAdE9nPUSQfK8rhYwKEVt_JyfEpl"
                                            asyncScriptOnLoad={this.onLoadRecaptcha}
                                            onChange={this.onVerify}
                                            onErrored={this.expiredRecaptcha}
                                        />
                                    </FormControl>
                                    <FormControl className={classes.formControl} >
                                        <Button
                                            style={{color:"#848391"}}
                                            onClick={this.handleSubmit}
                                        >Login</Button>
                                    </FormControl>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
 
const mapStateToProps = state => {
    return{
        isLogin : state.AuthReducer.statusLogin,
        validCaptcha : state.RecaptReducer.validate
    }
}

const mapDispatchToProps = dispatch => {
    return{
        doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin}),
        setTokenRecapt : (token) => dispatch({ type : Action.SET_RECAPT_TOKEN, payload : token}),
        validRecapt : () => dispatch({ type : Action.VALID_RECAPT})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme : true })(Login));