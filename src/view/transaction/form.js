import React, { Component } from 'react';
import _ from "lodash";
import { Autocomplete } from "@material-ui/lab";
import { withStyles } from "@material-ui/core/styles";
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import { Grid, Typography, Dialog,
    TextField, Paper, DialogTitle,
    FormHelperText, TableContainer, Table, TableHead,
    TableRow, TableBody, Button, DialogContentText,
    DialogContent, DialogActions, List, ListItem, ListItemText } 
    from "@material-ui/core";
import {connect} from "react-redux";

import { StyledTableCell, StyledTableRow } from "../../components/table";
import Action from "../../action";
import { ButtonKuning, ButtonHijau, 
    ButtonMerah,} from '../../components/button';
import priceConvert from '../../util/priceConvert';

const styles = theme => ({
    root : {
        padding : theme.spacing(4)
    },
    judul : {
        display : "inline-block",
        backgroundColor : "#c70039",
        padding : "5px 50px 5px 15px",
        borderTopRightRadius: "70px",
        borderBottomLeftRadius: "22px",
        fontWeight : "bold",
        color : "#fff5ea"
    },
    fieldForm : {
        marginBottom : "10px"
    },
    mt5 : {
        marginTop : "25px"
    },
    mt2 :{
        margin: "2px"
    },
    table: {
        minWidth: 650,
        "& .MuiTableCell-root": {
          borderLeft: "1px solid rgba(224, 224, 224, 1)"
        }
    }
});


class FormTransaction extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            products : [],
            selectedProduct : null,
            toggleQty : true,
            qtyProduct : 0,
            cartProduct : [],
            cartProductValid : true,
            totalProduct : 0,
            totalGrossAmount : 0,
            lineDiscount : 0,
            finalPromo : [],
            freeProduct : [],
            netAmount : 0,
            generated : false,
            openDialog : false,
            generateDialog : false,
            disabledFunc : false,
            buyer : {
                value : "",
                valid : true
            },
            description : {
                value : "",
                valid : true
            }
        }
        this.setValue = this.setValue.bind(this);
        this.addProduct = this.addProduct.bind(this);
        this.generateHanlde = this.generateHanlde.bind(this);
        this.setValueEdited = this.setValueEdited.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setValueObj = this.setValueObj.bind(this);
    }

    componentDidMount(){
        if (Object.keys(this.props.match.params).length > 0) {
            fetch(`http://localhost:8080/api/sales/transaction/${this.props.match.params.id}`,
            {
                method : "GET",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization" : "Bearer " + this.props.token
                }
            })
        .then(resp => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    throw new Error(`${text.message}`);
                })
            }
            return resp.json();
        })
        .then(json => {
            let products = json.products.map(el => ({...el, edited : false}));
            let totalGrossAmount = _.sumBy(products, (el) => {return el.qty*el.price;});
            let totalProduct = _.sumBy(products, "qty");
            let lineDisc = 0;
            let promotions = json.promotions;
            let freeProduct = [];

            promotions.forEach(val => {
                if (val.method === "Discount") {
                    if (val.whatCustGet[0].discount < 80) {
                        let purchase = 0;
                        val.whatCustBuy.forEach(el => {
                            let findProduct = json.products.find(elm => elm.idProduct === el.idProduct);
                            purchase += findProduct.price*findProduct.qty;
                        });
                        lineDisc += purchase*(val.whatCustGet[0].discount/100);
                    }else{
                        lineDisc += val.whatCustGet[0].discount
                    }
                    if (val.whatCustBuy.length === 1) {
                        let index = products.findIndex(item => item.idProduct === val.whatCustBuy[0].idProduct);
                        products[index] = {
                            ...products[index],
                            disc : val.whatCustGet[0].discount
                        }
                    }
                }else if(val.method === "Free Good"){
                    _.forEach(val.whatCustGet, el => {
                        freeProduct.push({
                            ...el.freeGood,
                            qty : el.qty
                        });
                        totalProduct += el.qty
                    })
                }
            });

            this.setState({
                buyer : {
                    ...this.state.buyer,
                    value : json.buyer
                },
                description : {
                    ...this.state.description,
                    value : json.description
                },
                cartProduct : products,
                totalProduct : totalProduct,
                totalGrossAmount : totalGrossAmount,
                finalPromo : json.promotions,
                freeProduct : freeProduct,
                lineDiscount : lineDisc,
                netAmount : totalGrossAmount - lineDisc,
                generated : true,
            })
        })
        .catch(err => {
            alert(err);
        })
        }

        fetch(`http://localhost:8080/api/sales/product`,
            {
                method : "GET",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization" : "Bearer " + this.props.token
                }
            })
        .then(resp => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    throw new Error(`${text.message}`);
                })
            }
            return resp.json();
        })
        .then(json => {
            this.setState({
                products : json.data
            })
        })
        .catch(err => {
            alert(err);
        })
    }

    setValueObj(el, key){
        let validate = true
        if (el.target.value.trim().length < 1) {
            validate = false
        }

        this.setState({
            [key] : {
                value : el.target.value,
                valid : validate
            }
        })
    }

    setValue(el, key){
        let value = el.target.value;
        if (key==="qtyProduct")
            value = parseInt(value)
        if (key==="qtyProduct" && value < 0) 
            value = 0;
        if (key==="qtyProduct" && value >= this.state.selectedProduct.qty) 
            value = this.state.selectedProduct.qty;

        this.setState({
            [key] : value
        })
    }

    setValueEdited(el, indx){
        let cart = this.state.cartProduct;
        let maxQty = this.state.products.find(el => el.idProduct === cart[indx].idProduct).qty;
        let value = parseInt(el.target.value);
        if (value < 0) {
            value = 0
        }else if(value > maxQty){
            value = maxQty
        }

        cart[indx] = {
            ...cart[indx],
            qty : value
        }

        this.setState({
            cartProduct : cart
        })
    }

    addProduct(){
        const {selectedProduct, qtyProduct,
            cartProduct} = this.state;
        let arrProduct = cartProduct;
        let findExistingProduct = selectedProduct && arrProduct.findIndex(el => el.idProduct === selectedProduct.idProduct);

        if (!selectedProduct) {
            alert("Select the product first");
        }else if (qtyProduct < 1 || qtyProduct > selectedProduct.qty){
            alert("make sure your Qty is valid");
        }else if (findExistingProduct >= 0){
            alert(`Product ${selectedProduct.productName} has already selected`);
        }else if (this.state.disabledFunc){
            alert("Submit edited product first")
        }
        else {
            let selectedObj = {
                ...selectedProduct,
                qty : qtyProduct,
                edited : false,
            }
            arrProduct.push(selectedObj);
            this.setState({
                cartProduct : arrProduct,
                selectedProduct : null,
                qtyProduct : 0,
                cartProductValid : arrProduct.length > 0 ? true : false,
                totalProduct : _.sumBy(arrProduct, "qty"),
                totalGrossAmount : _.sumBy(arrProduct, (el) => {return el.qty*el.price;}),
                generated : false,
                netAmount : 0,
                lineDiscount : 0,
                freeProduct : [],
                finalPromo : []
            })
        }
    }

    generateHanlde(){
        // console.log(this.state.cartProduct);
        let dataGenerate = {
            buyyer : "Silo",
            products : [...this.state.cartProduct]
        }
        if (this.state.cartProduct.length <= 0 || this.state.disabledFunc) {
            alert("Choose product first");
        }else{
            if (!this.state.generated) {
                fetch(`http://localhost:8080/api/sales/promotion/generate`,
                    {
                        method : "POST",
                        headers: {
                            "Content-type": "application/json; charset=UTF-8",
                            "Authorization" : "Bearer " + this.props.token
                        },
                        body : JSON.stringify(dataGenerate)
                    })
                .then(resp => {
                    if (!resp.ok) {
                        if (resp.status === 403){
                            return resp.json().then(text => {
                                alert(text.message);
                                this.props.logout();
                                this.props.history.push("/login");
                            })
                        }
                        return resp.json().then(text => {
                            throw new Error(`${text.message}`);
                        })
                    }
                    return resp.json();
                })
                .then(json => {
                    // console.log("json", json)
                    let filterPromo = []
                    let transaction = dataGenerate.products.map(el => el.idProduct);
                    let products = [...dataGenerate.products];
                    _.forEach(json, val => {
                        let promo = val.whatCustBuy.map(el => el.idProduct);
        
                        let validate = _.difference(promo, transaction);
                        if (validate.length === 0) {
                            let purchaseArr = []
                            _.forEach(promo, val => {
                                let findPurchase = dataGenerate.products.find((value) =>  value.idProduct === val)
                                if (findPurchase) {
                                    purchaseArr.push(findPurchase);
                                }
                            });
                            if (val.forPurchase <= _.sumBy(purchaseArr, (el) => {return el.qty*el.price;})) {
                                filterPromo.push(val)
                            }
                        }
                    })
                    
                    let validateProduct = [];
                    let validatePromo = [];
                    let generatedPromo = [];
                    let generatedProduct = [];
                    for (let i = 0; i < products.length; i++) {
                        let arrPromo = [];
                        for (let j = 0; j < filterPromo.length; j++) {
                            let status = false;
                            
                            if(filterPromo[j].whatCustBuy.find(el => el.idProduct === products[i].idProduct)){
                                status = true;
                            }
                            arrPromo.push({
                                "idPromotion" : filterPromo[j].idPromotion,
                                "status" : status
                            });
                        }
        
                        validateProduct.push({
                            ...products[i],
                            "promoStatus" : arrPromo,
                            "jumlah" : arrPromo.filter(el => el.status).length
                        })
                    }
        
                    for (let i = 0; i < filterPromo.length; i++) {
                        let arrProduk = [];
                        for (let k = 0; k < products.length; k++) {
                            let status = false;
                            if(filterPromo[i].whatCustBuy.find(el => el.idProduct === products[k].idProduct)){
                                status = true;
                            }
                            arrProduk.push({
                                "idProduct" : products[k].idProduct,
                                "status" : status
                            })
                        }
        
                        validatePromo.push({
                            ...filterPromo[i],
                            "productStatus" : arrProduk,
                            "jumlah" : arrProduk.filter(el => el.status).length
                        })
                        
                    }
        
                    _.forEach(validateProduct, element => {
                        if(element.promoStatus.filter(el => el.status).length === 1){
                            let jumlah = validatePromo.find(item => 
                                item.idPromotion === element.promoStatus.find(el => 
                                    el.status).idPromotion).jumlah;
                            if (jumlah === 1) {
                                generatedProduct.push(products.find(el => el.idProduct === element.idProduct))
                                generatedPromo.push(filterPromo.find(el => 
                                    el.idPromotion === element.promoStatus.find(el => el.status).idPromotion))
                            }
                        }
                    });
                    
                    validatePromo = _.differenceWith(validatePromo,generatedPromo, (valPromo, genPromo) => {
                        return valPromo.idPromotion === genPromo.idPromotion
                    });
                    validateProduct = _.differenceWith(validateProduct,generatedProduct, (valProduct, genProduct) => {
                        return valProduct.idProduct === genProduct.idProduct
                    })
                    validatePromo = _.orderBy(validatePromo, item => item.jumlah, ['desc']);

                    for (let idx = 0; idx < validatePromo.length; idx++) {
                        let diff = _.differenceWith(validatePromo[idx].whatCustBuy, generatedProduct, (valPromo, valProduct) => {
                            return valPromo.idProduct === valProduct.idProduct
                        });

                        if (diff.length === validatePromo[idx].whatCustBuy.length) {
                            generatedPromo = _.concat(generatedPromo, filterPromo.find(elm => elm.idPromotion===validatePromo[idx].idPromotion));
                            for (let i = 0; i < diff.length; i++) {
                                let temp = validateProduct.find(el => el.idProduct === diff[i].idProduct)
                                generatedProduct = _.concat(generatedProduct, temp);
                            }
                        }else{
                            for (let indx = 0; indx < generatedPromo.length; indx++) {
                                let diff = _.differenceWith(validatePromo[idx].whatCustBuy, generatedPromo[indx].whatCustBuy, (val1, val2) => {
                                    return val1.idProduct === val2.idProduct;
                                });
                                let change = false;
                                let mainMethod = "Free Good";
                                
                                if (diff.length < validatePromo[idx].whatCustBuy.length) {
                                    if (validatePromo[idx].method === mainMethod) {
                                        if (generatedPromo[indx].method === mainMethod) {
                                            let validateGet = validatePromo[idx].whatCustGet.map(value => {
                                                let qtyCheck = value.freeGood.qty;
                                                let checkProd = products.find(el => el.idProduct === value.freeGood.idProduct);
                                                if(checkProd){
                                                    qtyCheck -= checkProd.qty; 
                                                }
                                                return value.qty <= qtyCheck
                                            });
                                            if (_.differenceWith(validateGet, [true], _.isEqual).length === 0) {
                                                if (validatePromo[idx].whatCustBuy.length > generatedPromo[indx].whatCustBuy.length) {
                                                    change = true;
                                                }else if(validatePromo[idx].whatCustBuy.length === generatedPromo[indx].whatCustBuy.length){
                                                    if (validatePromo[idx].whatCustGet.length > generatedPromo[indx].whatCustGet.length) {
                                                        change = true;
                                                    }
                                                    else if (validatePromo[idx].whatCustGet.length === generatedPromo[indx].whatCustGet.length){
                                                        let promo1 = new Date() - new Date(validatePromo[idx].endDate);
                                                        let promo2 = new Date() - new Date(generatedPromo[indx].endDate);
                                                        if (promo1 < promo2) {
                                                            change = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }else{
                                            change = true;
                                        }
                                    }else{
                                        if (generatedPromo[indx].method !== mainMethod) {
                                            if (validatePromo[idx].whatCustBuy.length > generatedPromo[indx].whatCustBuy.length) {
                                                change = true;
                                            }else if(validatePromo[idx].whatCustBuy.length === generatedPromo[indx].whatCustBuy.length){
                                                let disc1 = validatePromo[idx].whatCustGet[0].discount;
                                                let disc2 = generatedPromo[indx].whatCustGet[0].discount;
                                                if (disc1 < 80) {
                                                    if (disc2 < 80) {
                                                        if (disc1 > disc2) {
                                                            change = true;   
                                                        }else if (disc1 === disc2){
                                                            let promo1 = new Date() - new Date(validatePromo[idx].endDate);
                                                            let promo2 = new Date() - new Date(generatedPromo[indx].endDate);
                                                            if (promo1 < promo2) {
                                                                change = true;
                                                            }
                                                        }
                                                    }
                                                    change = true;
                                                }else{
                                                    if (disc2 >= 80) {
                                                        if (disc1 > disc2) {
                                                            change = true;   
                                                        }else if (disc1 === disc2){
                                                            let promo1 = new Date() - new Date(validatePromo[idx].endDate);
                                                            let promo2 = new Date() - new Date(generatedPromo[indx].endDate);
                                                            if (promo1 < promo2) {
                                                                change = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }                         
                                }
                                if (change) {
                                    generatedPromo.splice(indx, 1);
                                    generatedPromo = _.concat(generatedPromo, validatePromo.find(elm => elm.idPromotion===validatePromo[idx].idPromotion));
                                    for (let i = 0; i < diff.length; i++) {
                                        let temp = validateProduct.find(el => el.idProduct === diff[i].idProduct)
                                        generatedProduct = _.concat(generatedProduct, temp);                                     
                                    }
                                }
                            }
                        }
                        
                    }
        
                    let freeProduct = this.state.freeProduct;
                    let totalProduct = this.state.totalProduct;
                    let lineDiscount = this.state.lineDiscount;
                    let cart = this.state.cartProduct;
        
                    _.forEach(generatedPromo, el => {   
                        if (el.method === "Free Good") {
                            _.forEach(el.whatCustGet, el => {
                                freeProduct.push({
                                    ...el.freeGood,
                                    qty : el.qty
                                });
                                totalProduct += el.qty
                            })
                        }else{
                            if (el.whatCustGet[0].discount < 80) {
                                let purchase = 0;
                                _.forEach(el.whatCustBuy, (item) => {
                                    let findProduct = products.find(el => el.idProduct === item.idProduct);
                                    purchase += findProduct.price*findProduct.qty;
                                });
                                lineDiscount += (purchase*(el.whatCustGet[0].discount/100));
                            }else{
                                lineDiscount += el.whatCustGet[0].discount;
                            }
                            if (el.whatCustBuy.length === 1) {
                                let index = cart.findIndex(item => item.idProduct === el.whatCustBuy[0].idProduct);
                                cart[index] = {
                                    ...cart[index],
                                    disc : el.whatCustGet[0].discount
                                }
                            }
                        }
                    })
        
                    this.setState({
                        finalPromo : generatedPromo,
                        freeProduct : freeProduct,
                        totalProduct : totalProduct,
                        lineDiscount : lineDiscount,
                        netAmount : this.state.totalGrossAmount - lineDiscount,
                        generated : true,
                        cartProduct : cart
                    })
                })
                .catch(err => {
                    alert(err);
                })
            }
            this.setState({
                generateDialog : true
            })
        }
    }

    handleSubmit(){
        const {
            finalPromo, 
            cartProduct,
            buyer,
            description,
            netAmount
        } = this.state;
        
        let products = cartProduct.map(el => {
            delete el.edited;
            return el
        })

        if (buyer.value.trim().length <= 0 || description.value.trim().length <= 0) {
            alert("unable to submit, please check the form");
            this.setState({
                buyer : {
                    ...buyer,
                    valid : buyer.value.trim().length <= 0 ? false : true
                },
                description : {
                    ...description,
                    valid : description.value.trim().length <= 0 ? false : true
                },
            })
        }else{
            let dataInput = {
                buyer : buyer.value,
                description : description.value,
                netAmount : netAmount,
                products : products,
                promotions : finalPromo
            }
            fetch(`http://localhost:8080/api/sales/transaction/${Object.keys(this.props.match.params).length > 0 ?
             `update/${this.props.match.params.id}` : "create"}`,
                {
                    method : Object.keys(this.props.match.params).length > 0 ? "PUT" : "POST",
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization" : "Bearer " + this.props.token
                    },
                    body : JSON.stringify(dataInput)
                })
            .then(resp => {
                if (!resp.ok) {
                    if (resp.status === 403){
                        return resp.json().then(text => {
                            alert(text.message);
                            this.props.logout();
                            this.props.history.push("/login");
                        })
                    }
                    return resp.json().then(text => {
                        throw new Error(`${text.message}`);
                    })
                }
                return resp.json();
            })
            .then(json => {
                alert(json.message);
                this.props.history.push("/admin/transaction")
            })
            .catch(err => {
                alert(err);
            })
        }
    }

    render() { 
        const {classes} = this.props;
        const {selectedProduct, products,
            qtyProduct, toggleQty, 
            cartProduct, cartProductValid,
            totalProduct, totalGrossAmount,
            lineDiscount, netAmount,
            generated, openDialog,
            freeProduct, generateDialog,
            finalPromo, buyer, description } = this.state;
        
        return (  
            <Grid container className={classes.root} spacing={5}>
                <Grid item xs={12} >
                    <Paper elevation={3}>
                        <Typography className={classes.judul} variant="h5">Header Transaction</Typography>
                        <Grid container spacing={3} className={classes.root} alignItems="center">
                            <Grid item xs={2}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Customer</Typography>
                            </Grid>
                            <Grid item xs={10}>
                                <TextField
                                    error={!buyer.valid}
                                    value={buyer.value}
                                    variant="outlined"
                                    fullWidth={true}
                                    onChange={(e)=>{this.setValueObj(e,"buyer")}}
                                />
                                { 
                                !buyer.valid &&
                                    <FormHelperText error>
                                        This field cannot be empty
                                    </FormHelperText>
                                }
                            </Grid>
                            <Grid item xs={2}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Description</Typography>
                            </Grid>
                            <Grid item xs={10}>
                                <TextField
                                    error={!description.valid}
                                    value={description.value}
                                    onChange={(e)=>{this.setValueObj(e,"description")}}
                                    variant="outlined"
                                    fullWidth={true}
                                    multiline
                                    rows={4}
                                />
                                { 
                                !description.valid &&
                                    <FormHelperText error>
                                        This field cannot be empty
                                    </FormHelperText>
                                }
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper elevation={3}>
                        <Typography className={classes.judul} variant="h5">Transaction Item</Typography>
                        <Grid container spacing={3} className={classes.root} alignItems="center">
                            <Grid item xs={2}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Product</Typography>
                            </Grid>
                            <Grid item xs={5}>
                                <Autocomplete
                                    id="combo-box-demo"
                                    value = {selectedProduct}
                                    onChange={(event, value) => {
                                        let qty = true;
                                        if(value !== null) qty=false;  
                                        this.setState({selectedProduct : value, toggleQty : qty})}}
                                    options={products}
                                    getOptionLabel={(option) => { return option.productName}}
                                    renderInput={(params) => { return <TextField {...params} label="Select Product" variant="outlined" fullWidth />}}
                                />
                            </Grid>
                            <Grid item xs={1}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Qty</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    disabled={toggleQty}
                                    type="number"
                                    min="0"
                                    variant="outlined"
                                    value={qtyProduct}
                                    onChange={(e) => {this.setValue(e,"qtyProduct")}}
                                    fullWidth={true}
                                />
                                {selectedProduct != null &&
                                <FormHelperText error>
                                    This field cannot be empty and cannot be more than {selectedProduct.qty}
                                </FormHelperText>}
                            </Grid>
                            <Grid item xs={2}></Grid>
                            <Grid item xs={10}>
                                <ButtonHijau onClick={this.addProduct}>
                                    Add
                                </ButtonHijau>
                            </Grid>
                            <Grid item xs={12} className={classes.mt5}>
                                <TableContainer component={Paper}>
                                    <Table className={classes.table} aria-label="customized table">
                                        <TableHead>
                                            <TableRow>
                                                <StyledTableCell align="center">Product</StyledTableCell>
                                                <StyledTableCell align="center">Price</StyledTableCell>
                                                <StyledTableCell align="center" style={{width:"356px"}}>Qty</StyledTableCell>
                                                <StyledTableCell align="center">Action</StyledTableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                        {
                                            cartProduct.length > 0 ? 
                                            cartProduct.map((row, indx) => (
                                                <StyledTableRow key={row.idProduct}>
                                                    <StyledTableCell scope="row">
                                                        {row.productName}
                                                    </StyledTableCell>
                                                    <StyledTableCell align="right">{priceConvert(row.price)}</StyledTableCell>
                                                    <StyledTableCell >
                                                        {row.edited ? 
                                                            <>
                                                                <TextField
                                                                    type="number"
                                                                    min="0"
                                                                    variant="outlined"
                                                                    value={row.qty}
                                                                    onChange={(e) => {this.setValueEdited(e,indx)}}
                                                                    fullWidth={true}
                                                                />
                                                                <FormHelperText error>
                                                                    This field cannot be empty and cannot be more than {
                                                                        products.find(el => el.idProduct === row.idProduct).qty
                                                                    }
                                                                </FormHelperText>
                                                            </>
                                                            : 
                                                            row.qty}
                                                    </StyledTableCell>
                                                    <StyledTableCell align="center">
                                                        {
                                                            !row.edited ?
                                                            <ButtonKuning
                                                                    className={classes.mt2} 
                                                                    size="small" 
                                                                    startIcon={<CreateIcon />} variant="contained"
                                                                    onClick={
                                                                        () => {
                                                                            let cart = cartProduct;
                                                                            let findProduct = products.find(el => el.idProduct === cart[indx].idProduct);
                                                                            let doEdit = false;
                                                                            if(cart[indx].price !== findProduct.price){
                                                                                if(window.confirm("The price has changed, please make a new transaction")){
                                                                                    this.history.push("/admin/add-transaction")
                                                                                }
                                                                            }else{
                                                                                doEdit = true;
                                                                            }

                                                                            if(doEdit){
                                                                                cart[indx] = {
                                                                                    ...cart[indx],
                                                                                    edited : !cart[indx].edited
                                                                                }
                                                                                let arrValidate = cart.map(el => el.edited);
                                                                                let valid = _.differenceWith(arrValidate, [false], _.isEqual);
                                                                                this.setState({
                                                                                    cartProduct : cart,
                                                                                    disabledFunc : valid.length === 0 ? false : true
                                                                                })
                                                                            }
                                                                        }
                                                                    }
                                                                >
                                                                Edit
                                                            </ButtonKuning> : 
                                                            <ButtonHijau
                                                                className={classes.mt2} 
                                                                size="small" 
                                                                startIcon={<CreateIcon />} variant="contained"
                                                                onClick={
                                                                    () => {
                                                                        let cart = cartProduct;
                                                                        let findProduct = products.find(el => el.idProduct === cart[indx].idProduct);
                                                                        cart[indx] = {
                                                                            ...cart[indx],
                                                                            price : findProduct.price,
                                                                            edited : !cart[indx].edited
                                                                        }
                                                                        let arrValidate = cart.map(el => el.edited);
                                                                        let valid = _.differenceWith(arrValidate, [false], _.isEqual);
                                                                        this.setState({
                                                                            cartProduct : cart,
                                                                            disabledFunc : valid.length === 0 ? false : true,
                                                                            cartProductValid : cart.length > 0 ? true : false,
                                                                            totalProduct : _.sumBy(cart, "qty"),
                                                                            totalGrossAmount : _.sumBy(cart, (el) => {return el.qty*el.price;}),
                                                                            generated : false,
                                                                            netAmount : 0,
                                                                            lineDiscount : 0,
                                                                            freeProduct : [],
                                                                            finalPromo : []
                                                                        })
                                                                    }
                                                                }
                                                            >
                                                                submit
                                                            </ButtonHijau>
                                                        }
                                                        <ButtonMerah size="small"
                                                                className={classes.mt2} 
                                                                startIcon={<DeleteIcon />} 
                                                                variant="outlined" 
                                                                color="secondary"
                                                                onClick={()=>{
                                                                    if(window.confirm("Are you sure you want to delete this record?")){
                                                                        let newArr = cartProduct;
                                                                        newArr.splice(indx, 1);
                                                                        this.setState({
                                                                            cartProduct : newArr,
                                                                            cartProductValid : newArr.length > 0 ? true : false,
                                                                            totalProduct : _.sumBy(newArr, "qty"),
                                                                            totalGrossAmount : _.sumBy(newArr, (el) => {return el.qty*el.price;}),
                                                                            generated : false,
                                                                            netAmount : 0,
                                                                            lineDiscount : 0,
                                                                            freeProduct : [],
                                                                            finalPromo : []
                                                                        })
                                                                    }
                                                                }}>
                                                            detele
                                                        </ButtonMerah>
                                                        
                                                    </StyledTableCell>
                                                </StyledTableRow>)) 
                                                : 
                                                
                                                <StyledTableRow>
                                                    <StyledTableCell colSpan="4">No product selected</StyledTableCell>
                                                </StyledTableRow>

                                        }
                                        {
                                            cartProduct.length > 0 &&
                                            <StyledTableRow backgroundColor="#e0e0e0">
                                                    <StyledTableCell colSpan="3" align="right">Total</StyledTableCell>
                                                    <StyledTableCell align="right">{
                                                        priceConvert(_.sumBy(cartProduct, el => {return el.qty*el.price}))
                                                    }</StyledTableCell>
                                            </StyledTableRow>
                                        }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                {!cartProductValid &&
                                    <FormHelperText error>
                                        Add at least one of product on table
                                    </FormHelperText>}
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid item xs={12} >
                    <Paper elevation={3}>
                        <Typography className={classes.judul} variant="h5">Detail Transaction</Typography>
                        <Grid container spacing={3} className={classes.root} alignItems="center">
                            <Grid item xs={2}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Total Product</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    disabled={true}
                                    variant="outlined"
                                    fullWidth={true}
                                    value={cartProduct.length}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Total Gross Amount</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    disabled={true}
                                    variant="outlined"
                                    fullWidth={true}
                                    value={priceConvert(totalGrossAmount)}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Total Casses</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    disabled={true}
                                    variant="outlined"
                                    fullWidth={true}
                                    value={totalProduct}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Total Discount</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    disabled={true}
                                    variant="outlined"
                                    fullWidth={true}
                                    value={priceConvert(lineDiscount)}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Typography variant="subtitle1" style={{fontWeight:"bold"}}>Net Amount</Typography>
                            </Grid>
                            <Grid item xs={10}>
                                <TextField
                                    disabled={true}
                                    variant="outlined"
                                    fullWidth={true}
                                    value={priceConvert(netAmount)}
                                />
                            </Grid>
                            <Grid item xs={12} container>
                                <Grid item xs={6} container spacing={1}>
                                    <Grid item>
                                        <ButtonKuning variant="contained" className={classes.mt2} onClick={this.generateHanlde}>Generate</ButtonKuning>
                                    </Grid>
                                    {
                                        generated &&
                                        <>
                                        <Grid item>
                                            <Button className={classes.mt2} color="primary" variant="contained" 
                                            onClick={() => {
                                                this.setState({
                                                    openDialog : true,
                                                })
                                            }}>Detail</Button>
                                        </Grid>
                                        <Grid item>
                                            <ButtonHijau className={classes.mt2} 
                                                variant="contained"
                                                onClick={this.handleSubmit}>
                                                Checkout
                                            </ButtonHijau>
                                        </Grid>
                                        </>
                                    }
                                </Grid>
                                <Grid item xs={6} container spacing={1} justify="flex-end">
                                    <ButtonMerah variant="contained" className={classes.mt2} 
                                        onClick={() => {
                                            if(window.confirm("Are you sure to leave this page ?"))
                                                this.props.history.push("/admin/transaction")
                                        }}
                                    >
                                        Cencel
                                    </ButtonMerah>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                {
                    generated &&
                    <>
                        <Dialog
                            fullWidth={true}
                            maxWidth="lg"
                            scroll="paper"
                            open={openDialog}
                            onClose={() => {
                                this.setState({
                                    openDialog : false,
                                    dataDetail : {}
                                });
                            }}
                            aria-labelledby="max-width-dialog-title"
                        >
                            <DialogTitle id="max-width-dialog-title">Detail Transaction</DialogTitle>
                            <DialogContent>
                                {/* <DialogContentText>
                                    You can set my maximum width and whether to adapt or not.
                                </DialogContentText> */}
                                <Grid container className={classes.root}>
                                    <Grid item container spacing={3}>
                                        <TableContainer component={Paper}>
                                            <Table className={classes.table} aria-label="customized table">
                                                <TableHead>
                                                    <TableRow>
                                                        <StyledTableCell align="center">Qty</StyledTableCell>
                                                        <StyledTableCell align="center">Product</StyledTableCell>
                                                        <StyledTableCell align="center">Price</StyledTableCell>
                                                        <StyledTableCell align="center">Sub Total</StyledTableCell>
                                                        <StyledTableCell align="center">Disc</StyledTableCell>
                                                        <StyledTableCell align="center">Total</StyledTableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                {
                                                    cartProduct.length > 0 ? 
                                                        cartProduct.map((row, indx) => (
                                                            <StyledTableRow key={row.idProduct}>
                                                                <StyledTableCell >{row.qty}</StyledTableCell>
                                                                <StyledTableCell scope="row">
                                                                    {row.productName}
                                                                </StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(row.price)}</StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(row.price*row.qty)}</StyledTableCell>
                                                                <StyledTableCell align={row.disc && row.disc > 80 ? "right" : "center"}>{
                                                                    row.disc ?
                                                                    row.disc > 80 ?
                                                                            priceConvert(row.disc)
                                                                        :
                                                                            row.disc + "%"
                                                                    : "-"
                                                                }</StyledTableCell>
                                                                <StyledTableCell align="right">
                                                                    {row.disc ? 
                                                                        row.disc < 80 ? priceConvert((row.price*row.qty)-((row.price*row.qty)*(row.disc/100))) :
                                                                            priceConvert((row.price*row.qty)-row.disc) : 
                                                                        priceConvert(row.price*row.qty)}
                                                                </StyledTableCell>
                                                            </StyledTableRow>)) 
                                                        : 
                                                        
                                                        <StyledTableRow>
                                                            <StyledTableCell colSpan="5">No product selected</StyledTableCell>
                                                        </StyledTableRow>

                                                }
                                                {
                                                    freeProduct.length > 0 &&
                                                        freeProduct.map((el,indx) => (
                                                            <StyledTableRow key={el.idProduct}>
                                                                <StyledTableCell >{el.qty}</StyledTableCell>
                                                                <StyledTableCell scope="row">
                                                                    {el.productName}
                                                                </StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                <StyledTableCell align="center">-</StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                            </StyledTableRow>
                                                        ))
                                                }
                                                {
                                                    finalPromo.length > 0 &&
                                                        finalPromo.map((el,indx) => {
                                                            if(el.whatCustBuy.length > 1 && el.method === "Discount"){
                                                                let desc = "Buy ";
                                                                let purchase = 0;
                                                                let item = el.whatCustBuy.map(el => el.productName);
                                                                el.whatCustBuy.forEach(item => {
                                                                    let findProduct = cartProduct.find(elm => elm.idProduct === item.idProduct);
                                                                    purchase += findProduct.price*findProduct.qty;
                                                                })

                                                                desc += item.join(" & ");
                                                                desc += ` for purchase ${priceConvert(el.forPurchase)} , total purchase : ${priceConvert(purchase)}`;
                                                                return (
                                                                    <StyledTableRow key={el.idPromotion}>
                                                                        <StyledTableCell colSpan="4">
                                                                            {desc}
                                                                        </StyledTableCell>
                                                                        <StyledTableCell align="center">
                                                                            {el.whatCustGet[0].discount >= 80 ? 
                                                                                priceConvert(el.whatCustGet[0].discount) : 
                                                                                el.whatCustGet[0].discount+"%"
                                                                            }
                                                                        </StyledTableCell>
                                                                        <StyledTableCell align="right">{
                                                                            el.whatCustGet[0].discount >= 80 ? 
                                                                                priceConvert(0 - (purchase - el.whatCustGet[0].discount)) : 
                                                                                "-(" + priceConvert((purchase - (purchase*(el.whatCustGet[0].discount/100))))+" )"
                                                                        }</StyledTableCell>
                                                                    </StyledTableRow>
                                                                )
                                                            }
                                                        })
                                                }
                                                {   
                                                    <>
                                                    <StyledTableRow backgroundColor="#e0e0e0">
                                                            <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Gross Amount</StyledTableCell>
                                                            <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                                priceConvert(_.sumBy(cartProduct, el => {return el.qty*el.price}))
                                                            }</StyledTableCell>
                                                    </StyledTableRow>
                                                    <StyledTableRow backgroundColor="#e0e0e0">
                                                            <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Discount</StyledTableCell>
                                                            <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                                priceConvert(lineDiscount)
                                                            }</StyledTableCell>
                                                    </StyledTableRow>
                                                    <StyledTableRow backgroundColor="#e0e0e0">
                                                            <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Net Amount</StyledTableCell>
                                                            <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                                priceConvert(netAmount)
                                                            }</StyledTableCell>
                                                    </StyledTableRow>
                                                    </>
                                                }
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Grid>
                                </Grid>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={() => {
                                        this.setState({
                                            openDialog : false,
                                            dataDetail : {}
                                        });
                                    }} color="primary">
                                    Close
                                </Button>
                            </DialogActions>
                        </Dialog>
                        <Dialog
                            fullWidth={true}
                            maxWidth="lg"
                            scroll="paper"
                            open={generateDialog}
                            onClose={() => {
                                this.setState({
                                    generateDialog : false,
                                    dataDetail : {}
                                });
                            }}
                            aria-labelledby="max-width-dialog-title"
                        >
                            <DialogTitle id="max-width-dialog-title">Detail Promotion</DialogTitle>
                            <DialogContent>
                                {
                                    finalPromo.length < 1 ?
                                    <DialogContentText>
                                        There is no promotion
                                    </DialogContentText> : 
                                    <Grid container className={classes.root}>
                                        <Grid item container spacing={3}>
                                            {
                                                finalPromo.length > 0 &&
                                                finalPromo.map((el, indx) => {
                                                    let discount = 0;
                                                    if (el.method === "Discount") {
                                                        if (el.whatCustGet[0].discount < 80) {
                                                            let purchase = 0;
                                                            _.forEach(el.whatCustBuy, (item) => {
                                                                let findProduct = cartProduct.find(el => el.idProduct === item.idProduct);
                                                                purchase += findProduct.price*findProduct.qty;
                                                            });
                                                            discount = (purchase*(el.whatCustGet[0].discount/100));
                                                        }else{
                                                            discount = el.whatCustGet[0].discount;
                                                        }
                                                    }
                                                    return(
                                                        <Grid item xs={12}>
                                                            <Paper elevation={3}>
                                                                <Typography className={classes.judul} variant="subtitle1">Promotion - {indx + 1}</Typography>
                                                                <Grid container className={classes.root}>
                                                                    <Typography variant="subtitle2">Type = {el.method}</Typography>
                                                                    <TableContainer component={Paper}>
                                                                        <Table className={classes.table} aria-label="customized table">
                                                                            <TableHead>
                                                                                <TableRow>
                                                                                    <StyledTableCell style={{width:"40%"}} align="center">What Customer Buys</StyledTableCell>
                                                                                    <StyledTableCell style={{width:"40%"}} align="center">Actual</StyledTableCell>
                                                                                    <StyledTableCell style={{width:"20%"}} align="center">What Customer Get</StyledTableCell>
                                                                                </TableRow>
                                                                            </TableHead>
                                                                            <TableBody>
                                                                                <StyledTableRow>
                                                                                    <StyledTableCell >
                                                                                        <Grid container>
                                                                                            <Grid item xs={12} container>
                                                                                                <Grid item xs={4}><Typography>Item(s)</Typography></Grid>
                                                                                                <Grid item xs={1}><Typography>:</Typography></Grid>
                                                                                                <Grid item xs={7}>
                                                                                                    {
                                                                                                        el.whatCustBuy.map((elm, indx) => (
                                                                                                            <Typography key={indx}>- {elm.productName}</Typography>
                                                                                                        ))
                                                                                                    }
                                                                                                </Grid>
                                                                                                <Grid item xs={4}><Typography>For Purchase</Typography></Grid>
                                                                                                <Grid item xs={1}><Typography>:</Typography></Grid>
                                                                                                <Grid item xs={7}>
                                                                                                    <Typography>{priceConvert(el.forPurchase)}</Typography>
                                                                                                </Grid>
                                                                                            </Grid>
                                                                                        </Grid>
                                                                                    </StyledTableCell>
                                                                                    <StyledTableCell scope="row">
                                                                                        <Grid container>
                                                                                            <List>    
                                                                                            {
                                                                                                el.whatCustBuy.map((elm, indx) => {
                                                                                                    let findProduct = cartProduct.find(el=> el.idProduct === elm.idProduct)
                                                                                                    return(
                                                                                                        <ListItem key={indx}>
                                                                                                            <ListItemText>
                                                                                                                <Typography>{indx+1}. {findProduct.productName}</Typography>
                                                                                                                <Typography>   {priceConvert(findProduct.price*findProduct.qty)}</Typography>
                                                                                                            </ListItemText>
                                                                                                        </ListItem>
                                                                                                )})
                                                                                            }
                                                                                            </List>
                                                                                            
                                                                                            
                                                                                        </Grid>
                                                                                    </StyledTableCell>
                                                                                    <StyledTableCell>
                                                                                        {
                                                                                            el.method === "Free Good" ? 
                                                                                            el.whatCustGet.map((item, indx) => (
                                                                                                <Typography key={indx}>{`${item.freeGood.productName} @${item.qty} psc`}</Typography>
                                                                                            )) 
                                                                                            : 
                                                                                            <Typography>Discount {priceConvert(discount)}</Typography>
                                                                                        }
                                                                                    </StyledTableCell>
                                                                                </StyledTableRow>
                                                                            </TableBody>
                                                                        </Table>
                                                                    </TableContainer>
                                                                </Grid>
                                                            </Paper>
                                                        </Grid>
                                                    )
                                                })
                                            }
                                        </Grid>
                                    </Grid>
                                }
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={() => {
                                        this.setState({
                                            generateDialog : false,
                                            dataDetail : {}
                                        });
                                    }} color="primary">
                                    Close
                                </Button>
                            </DialogActions>
                        </Dialog>
                    </>
                }
            </Grid>
            
        );
    }
}


const mapStateToProps = state => {
    return{
        token : state.AuthReducer.token
    }
}

const mapDispatchToProps = dispatch => {
    return{
        doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin}),
        setTokenRecapt : (token) => dispatch({ type : Action.SET_RECAPT_TOKEN, payload : token}),
        validRecapt : () => dispatch({ type : Action.VALID_RECAPT}),
        logout : () => dispatch({ type : Action.LOGOUT})
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, {withTheme : true})(FormTransaction));