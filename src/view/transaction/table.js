import React, { Component } from 'react';
import {connect} from "react-redux";
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import DeleteIcon from "@material-ui/icons/Delete";
import CreateIcon from '@material-ui/icons/Create';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Pagination from "@material-ui/lab/Pagination"
import { Select, Grid, IconButton,
    Typography, MenuItem, FormControl,
    InputLabel, Input, InputAdornment,
    Button, Table, TableBody, TableRow,
    TableHead, TableContainer, Tooltip, 
    Dialog, DialogActions, DialogContent, 
    DialogTitle, Paper
} from "@material-ui/core";
import moment from "moment";
import { withStyles } from "@material-ui/core/styles";
import _ from "lodash";
import ReactToPrint from 'react-to-print';

import { StyledTableCell, StyledTableRow } from "../../components/table";
import { ButtonKuning, ButtonHijau } from "../../components/button";
import Action from "../../action";
import priceConvert from "../../util/priceConvert";
import PrintTransaction from "../../view/pdf/transaction-pdf"

const styles = theme => ({
    root : {
        padding : theme.spacing(4)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    mt2 :{
        margin: "0.4vh"
    },
    table: {
        minWidth: 650,
        "& .MuiTableCell-root": {
          borderLeft: "1px solid rgba(224, 224, 224, 1)"
        }
    },
    th : {
        backgroundColor: "#1e212d",
        color: theme.palette.common.white
    }
});

class TableTransaction extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            keyword : "",
            limit : 5,
            currentPage : 1,
            page : 0,
            data : [],
            dataDetail : {},
            openDetail : false,
            cellProduct : 0,
        }
        // this.componentRef = useRef();
        // this.handlePrint = useReactToPrint({
        //     content: () => this.componentRef.current,
        // });
        this.handleChange = this.handleChange.bind(this);
        this.searchHandle = this.searchHandle.bind(this);
        this.handlePage = this.handlePage.bind(this);
        this.searchHandleBtn = this.searchHandleBtn.bind(this);
        this.getData = this.getData.bind(this);
        this.handleDetail = this.handleDetail.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount(){
        const { limit } = this.state;
        fetch(`http://localhost:8080/api/sales/transaction?limit=${limit}&offset=0`,
        {
            method : "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization" : "Bearer " + this.props.token
            }
        })
        .then(resp => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    // console.log(text);
                    throw new Error(`${text.message}`);
                })
            }
            return resp.json();
        })
        .then(json => {
            this.setState({
                data : json.data,
                page : Math.ceil(json.size/this.state.limit)
            })
        })
        .catch(err => {
            alert(err);
        })
    }

    handleChange(el, key){
        // console.log(el.target);
        this.setState({
            [key] : el.target.value
        }, () => {
            this.getData();
        })
    }

    searchHandle(el){
        if(el.keyCode === 13){
            this.setState({
                keyword : el.target.value
            }, () => {
                this.getData();
            });
        }
    }

    handlePage(event, value){
        this.setState({
            currentPage : value
        }, () => {
            this.getData();
        })
    }

    handleDetail(value){
        let data = value;
        let lineDisc = 0;
        let netAmount = 0;
        let totalAmount = 0;
        let promotions = data.promotions;
        let cellProduct = data.products.length;
        
        data.products.forEach((value, index) => {
            totalAmount += value.price*value.qty;
        });

        promotions.forEach(val => {
            if (val.method === "Discount") {
                if (val.whatCustGet[0].discount < 80) {
                    let purchase = 0;
                    val.whatCustBuy.forEach(el => {
                        let findProduct = data.products.find(elm => elm.idProduct === el.idProduct);
                        purchase += findProduct.price*findProduct.qty;
                    });
                    lineDisc += purchase*(val.whatCustGet[0].discount/100);
                }else{
                    lineDisc += val.whatCustGet[0].discount
                }
                if (val.whatCustBuy.length === 1) {
                    let index = data.products.findIndex(item => item.idProduct === val.whatCustBuy[0].idProduct);
                    data.products[index] = {
                        ...data.products[index],
                        disc : val.whatCustGet[0].discount
                    }
                }else if(val.whatCustBuy.length > 2){
                    cellProduct += 1;
                }
            }else if(val.method === "Free Good"){
                cellProduct += val.whatCustGet.length;
            }
        });

        netAmount = totalAmount - lineDisc;
        data = {
            ...data,
            totalAmount,
            lineDisc,
            promotions : _.orderBy(data.promotions, elm => elm.method, ['desc'])   
        }
        this.setState({
            dataDetail : data,
            openDetail : true,
            cellProduct : cellProduct
        });
    }

    searchHandleBtn(){
        this.getData();
    }

    getData(){
        const { limit, page, currentPage, keyword } = this.state;
        let query = "";
        let arrQuery = [];
        arrQuery.push(`limit=${limit}`);
        arrQuery.push(`offset=${(limit*currentPage) - limit}`);

        keyword.length > 0 && 
            arrQuery.push(`idTransaction=${keyword}`);
        query = arrQuery.join("&");

        fetch(`http://localhost:8080/api/sales/transaction?${query}`,
        {
            method : "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization" : "Bearer " + this.props.token
            }
        })
        .then(resp => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    throw new Error(`${text.message}`);
                })
            }
            return resp.json();
        })
        .then(json => {
            this.setState({
                data : json.data,
                page : Math.ceil(json.size/this.state.limit),
            })
        })
        .catch(err => {
            alert(err);
        })
    }

    handleDelete(idTransaction){
        fetch(`http://localhost:8080/api/sales/transaction/delete/${idTransaction}`,
        {
            method : "DELETE",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization" : "Bearer " + this.props.token
            }
        })
        .then(resp => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    // console.log(text);
                    throw new Error(`${text.message}`);
                })
            }
            return resp.json();
        })
        .then(json => {
            alert(json.message);
        })
        .catch(err => {
            alert(err);
        })
    }

    render() { 
        const { classes, history } = this.props
        const { keyword, limit, currentPage, data,
            page, dataDetail, openDetail, cellProduct } = this.state;
        const pageStyle = `
          @media all {
            .page-break {
              display: none;
            }
          }
          
          @media print {
            html, body {
              height: initial !important;
              overflow: initial !important;
              -webkit-print-color-adjust: exact;
            }
          }
          
          @media print {
            .page-break {
              margin-top: 2rem;
              display: block;
              page-break-before: auto;
            }
          }
          
          @page {
            size: auto;
            margin: 50mm;
          }
        `;        
        return (
            <Grid container className={classes.root}>
                <Grid item xs={12} container spacing={3} alignItems="center">
                    <Grid item xs={12} style={{textAlign:"center"}}>
                        <Typography variant="h3" style={{fontWeight:"500"}}> Transaction Table </Typography>
                    </Grid>
                    
                    <Grid item xs={4}>
                        <FormControl fullWidth={true}>
                            <InputLabel>Search Transaction Code</InputLabel>
                            <Input
                                type="text"
                                onChange={(e)=> {
                                    this.setState({
                                        keyword : e.target.value
                                    },() => {
                                        e.target.value.length === 0 &&
                                            this.getData()
                                    })
                                }}
                                onKeyUp={this.searchHandle}
                                value={keyword}
                                endAdornment={
                                <InputAdornment position="end">
                                    <IconButton onClick={this.searchHandleBtn}>
                                        <SearchIcon/>
                                    </IconButton>
                                </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={4}></Grid>
                    <Grid container item xs={4} justify="flex-end">
                        <ButtonHijau startIcon={<AddIcon />} onClick={() => {history.push("/admin/add-transaction")}}>
                            Add Transaction
                        </ButtonHijau>
                    </Grid>
                    <Grid item xs={12}>
                        <TableContainer>
                            <Table aria-label="customized table" className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell align="center">Transaction Code</StyledTableCell>
                                        <StyledTableCell align="center">Description</StyledTableCell>
                                        <StyledTableCell align="center">Date</StyledTableCell>
                                        <StyledTableCell align="center">Net Amount</StyledTableCell>
                                        <StyledTableCell align="center">Action</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        data.length > 0 ? 
                                        data.map((el,indx) => {
                                            return (
                                                <StyledTableRow key={indx}>
                                                    <StyledTableCell component="th" scope="row">{el.idTransaction}</StyledTableCell>
                                                    <StyledTableCell>{el.description}</StyledTableCell>
                                                    <StyledTableCell>{moment(el.createdDate).format("YYYY-MM-DD")}</StyledTableCell>
                                                    <StyledTableCell align="right">{priceConvert(el.netAmount)}</StyledTableCell>
                                                    <StyledTableCell align="center">
                                                        <Tooltip title="Detail" arrow>
                                                            <Button className={classes.mt2} size="small" variant="contained" color="primary"
                                                                startIcon={<ErrorOutlineIcon/>} onClick={() => this.handleDetail(el)}>
                                                                Detail
                                                            </Button>
                                                        </Tooltip>
                                                        {
                                                            moment(el.createdDate).format("YYYY-MM-DD") === moment().format("YYYY-MM-DD") &&
                                                            <>
                                                                <Tooltip title="Edit" arrow>
                                                                    <ButtonKuning className={classes.mt2} size="small" 
                                                                        startIcon={<CreateIcon />} variant="contained" 
                                                                        color="secondary" onClick={() => {this.props.history.push("/admin/edit-transaction/"+el.idTransaction)}}>
                                                                        Edit
                                                                    </ButtonKuning>
                                                                </Tooltip>
                                                                <Tooltip title="Delete" arrow>
                                                                    <Button className={classes.mt2} size="small" 
                                                                        startIcon={<DeleteIcon />} variant="contained" 
                                                                        color="secondary" 
                                                                        onClick ={()=> {
                                                                            if(window.confirm("Are you sure to delete this data ?")){
                                                                                this.handleDelete(el.idTransaction)
                                                                            }
                                                                        }}>
                                                                        detele
                                                                    </Button>
                                                                </Tooltip>
                                                            </>

                                                        }
                                                        
                                                    </StyledTableCell>
                                                </StyledTableRow>
                                            )
                                        }) :
                                                <StyledTableRow>
                                                    <StyledTableCell colSpan={5} align="center">
                                                        No data available
                                                    </StyledTableCell>
                                                </StyledTableRow>
                                    }
                                    
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                    <Grid item container xs={12} justify="center">
                        <Grid item container xs={2} spacing={3} alignItems="center">
                            <Grid item>
                                <Typography >Show : </Typography>
                            </Grid>
                            <Grid item>
                                <FormControl>
                                    <Select
                                        value={limit}
                                        onChange={(el) => {this.handleChange(el, "limit")}}
                                        inputProps={{
                                            name: 'limit',
                                        }}
                                    >
                                        <MenuItem value={5}>5</MenuItem>
                                        <MenuItem value={10}>10</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid item container xs={10} justify="flex-end">
                        {page > 1 &&     
                            <Pagination count={page} page={currentPage} onChange={this.handlePage}/>
                        }
                        </Grid>
                    </Grid>
                    {
                        Object.keys(dataDetail).length > 0 &&
                        <Dialog
                            fullWidth={true}
                            maxWidth="lg"
                            scroll="paper"
                            open={openDetail}
                            onClose={() => {
                                this.setState({
                                    openDetail : false,
                                    dataDetail : {}
                                });
                            }}
                            aria-labelledby="max-width-dialog-title"
                        >
                            <DialogTitle id="max-width-dialog-title">Detail Transaction</DialogTitle>
                            <DialogContent>
                                <Grid container className={classes.root} >
                                    <Grid item container spacing={3}>
                                        <Grid item xs={12} container alignItems="flex-end" justify="center" style={{borderBottom:"1px solid black"}}>
                                            <Typography variant="h3" style={{fontWeight:500}}>Invoice</Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Grid container alignItems="flex-start">
                                                <Grid item xs={8} container spacing={1}>
                                                    <Grid item xs={3}>Invoice No</Grid>
                                                    <Grid item xs={1}>:</Grid>
                                                    <Grid item xs={8}>{dataDetail.idTransaction}</Grid>
                                                    <Grid item xs={3}>Desciption</Grid>
                                                    <Grid item xs={1}>:</Grid>
                                                    <Grid item xs={8}>{dataDetail.description}</Grid>
                                                </Grid>
                                                <Grid item xs={4} container spacing={1} >
                                                    <Grid item xs={3}>Date</Grid>
                                                    <Grid item xs={1}>:</Grid>
                                                    <Grid item xs={8}>{dataDetail.createdDate}</Grid>
                                                    <Grid item xs={3}>Buyer</Grid>
                                                    <Grid item xs={1}>:</Grid>
                                                    <Grid item xs={8}>{dataDetail.buyer}</Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TableContainer component={Paper}>
                                                <Table className={classes.table} aria-label="customized table">
                                                    <StyledTableCell align="center" style={{color: "#fff", backgroundColor: "#1e212d"}}>Qty</StyledTableCell>
                                                    <StyledTableCell align="center" style={{color: "#fff", backgroundColor: "#1e212d"}}>Product</StyledTableCell>
                                                    <StyledTableCell align="center" style={{color: "#fff", backgroundColor: "#1e212d"}}>Price</StyledTableCell>
                                                    <StyledTableCell align="center" style={{color: "#fff", backgroundColor: "#1e212d"}}>Sub Total</StyledTableCell>
                                                    <StyledTableCell align="center" style={{color: "#fff", backgroundColor: "#1e212d"}}>Disc</StyledTableCell>
                                                    <StyledTableCell align="center" style={{color: "#fff", backgroundColor: "#1e212d"}}>Total</StyledTableCell>
                                                    <TableBody>
                                                    {
                                                        dataDetail.products.length > 0 ? 
                                                        dataDetail.products.map((row, indx) => {
                                                            return (
                                                                <>
                                                                <StyledTableRow key={row.idProduct}>
                                                                    <StyledTableCell >{row.qty}</StyledTableCell>
                                                                    <StyledTableCell component="th" scope="row">
                                                                        {row.productName}
                                                                    </StyledTableCell>
                                                                    <StyledTableCell align="right">{priceConvert(row.price)}</StyledTableCell>
                                                                    <StyledTableCell align="right">{priceConvert(row.price*row.qty)}</StyledTableCell>
                                                                    <StyledTableCell align={row.disc && row.disc > 80 ? "right" : "center"}>{
                                                                        row.disc ?
                                                                        row.disc > 80 ?
                                                                                priceConvert(row.disc)
                                                                            :
                                                                                row.disc + "%"
                                                                        : "-"
                                                                    }</StyledTableCell>
                                                                    <StyledTableCell align="right">
                                                                        {row.disc ? 
                                                                            row.disc < 80 ? priceConvert((row.price*row.qty)-((row.price*row.qty)*(row.disc/100))) :
                                                                                priceConvert((row.price*row.qty)-row.disc) : 
                                                                            priceConvert(row.price*row.qty)}
                                                                    </StyledTableCell>
                                                                </StyledTableRow>
                                                                </>
                                                            )
                                                        }) 
                                                            : 
                                                            
                                                            <StyledTableRow>
                                                                <StyledTableCell colSpan="5">No product selected</StyledTableCell>
                                                            </StyledTableRow>

                                                    }
                                                    {
                                                        dataDetail.promotions.length > 0 &&
                                                            dataDetail.promotions.map((el,indx) => {
                                                                if(el.method === "Free Good"){
                                                                    let freeGood = el.whatCustGet.map(elm => {
                                                                        return(
                                                                            <StyledTableRow key={elm.idWcg}>
                                                                                <StyledTableCell >{elm.qty}</StyledTableCell>
                                                                                <StyledTableCell component="th" scope="row">
                                                                                    {elm.freeGood.productName}
                                                                                </StyledTableCell>
                                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                                <StyledTableCell align="center">-</StyledTableCell>
                                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                            </StyledTableRow>
                                                                        )
                                                                    })
                                                                    return freeGood;
                                                                }else if(el.method === "Discount" && el.whatCustBuy.length > 1){
                                                                    let desc = "Buy ";
                                                                    let purchase = 0;
                                                                    let item = el.whatCustBuy.map(el => el.productName);
                                                                    el.whatCustBuy.forEach(item => {
                                                                        let findProduct = dataDetail.products.find(elm => elm.idProduct === item.idProduct);
                                                                        purchase += findProduct.price*findProduct.qty;
                                                                    })

                                                                    desc += item.join(" & ");
                                                                    desc += ` for purchase ${priceConvert(el.forPurchase)} , total purchase : ${priceConvert(purchase)}`;
                                                                    return (
                                                                        <StyledTableRow key={el.idPromotion}>
                                                                            <StyledTableCell colSpan="4">
                                                                                {desc}
                                                                            </StyledTableCell>
                                                                            <StyledTableCell align="center">
                                                                                {el.whatCustGet[0].discount >= 80 ? 
                                                                                    priceConvert(el.whatCustGet[0].discount) : 
                                                                                    el.whatCustGet[0].discount+"%"
                                                                                }
                                                                            </StyledTableCell>
                                                                            <StyledTableCell align="right">{
                                                                                el.whatCustGet[0].discount >= 80 ? 
                                                                                    priceConvert(0 - (purchase - el.whatCustGet[0].discount)) : 
                                                                                    "-(" + priceConvert((purchase*(el.whatCustGet[0].discount/100)))+" )"
                                                                            }</StyledTableCell>
                                                                        </StyledTableRow>
                                                                    )
                                                                }
                                                            })
                                                    }
                                                    {   
                                                        <>
                                                        {cellProduct%8 === 0 && <div className="page-break" />}
                                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Gross Amount</StyledTableCell>
                                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                                    priceConvert(_.sumBy(dataDetail.products, el => {return el.qty*el.price}))
                                                                }</StyledTableCell>
                                                        </StyledTableRow>
                                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Discount</StyledTableCell>
                                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                                    priceConvert(dataDetail.lineDisc)
                                                                }</StyledTableCell>
                                                        </StyledTableRow>
                                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Net Amount</StyledTableCell>
                                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                                    priceConvert(dataDetail.netAmount)
                                                                }</StyledTableCell>
                                                        </StyledTableRow>
                                                        </>
                                                    }
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <div style={{display:"none"}}>
                                    <PrintTransaction data={dataDetail} ref={el => (this.componentRef = el)}/>
                                </div>
                            </DialogContent>
                            <DialogActions>
                                <ReactToPrint
                                    trigger={() => {
                                        // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
                                        // to the root node of the returned component as it will be overwritten.
                                        return (<Button color="primary" variant="contained"
                                                    // onClick={() => {
                                                    //     const win = window.open("/print-invoice", "_blank");
                                                    //     win.focus();
                                                    // }}
                                                >
                                                    Print
                                                </Button>);
                                    }}
                                    content={() => this.componentRef}
                                    pageStyle = {pageStyle}
                                    documentTitle = {`Transaction_${dataDetail.idTransaction}_${dataDetail.createdDate}`}
                                />
                                
                                <Button onClick={() => {
                                        this.setState({
                                            openDetail : false,
                                            dataDetail : {}
                                        });
                                    }} color="secondary" variant="contained">
                                    Close
                                </Button>
                            </DialogActions>
                        </Dialog>
                    }
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return{
        isLogin : state.AuthReducer.statusLogin,
        validCaptcha : state.RecaptReducer.validate,
        token : state.AuthReducer.token
    }
}

const mapDispatchToProps = dispatch => {
    return{
        doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin}),
        setTokenRecapt : (token) => dispatch({ type : Action.SET_RECAPT_TOKEN, payload : token}),
        validRecapt : () => dispatch({ type : Action.VALID_RECAPT}),
        logout : () => dispatch({ type : Action.LOGOUT})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, {withTheme:true})(TableTransaction));