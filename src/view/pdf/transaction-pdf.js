import React, { Component } from 'react';
import _ from "lodash";
import moment from "moment";
import { 
    withStyles, Grid,
    Typography, Paper, 
    TableContainer, Table, 
    TableBody, TableHead, 
    TableRow, GridList, Box  
} from "@material-ui/core";

import { StyledTableCell, StyledTableRow } from "../../components/table";
import priceConvert from "../../util/priceConvert";

const styles = theme => ({
    root : {
        padding : theme.spacing(4)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    mt2 :{
        margin: "0.4vh"
    },
    table: {
        minWidth: 650,
        "& .MuiTableCell-root": {
          borderLeft: "1px solid rgba(224, 224, 224, 1)"
        }
    },
    th : {
        backgroundColor: "#1e212d",
        color: theme.palette.common.white
    }
});

class PrintTransaction extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            dataDetail : {},
            openDetail : false,
            cellProduct : 0,
        }
    }

    componentDidMount(){

    }

    drawTable(products, classes){
        let lineDisc = 0;
        let grossAmount = _.sumBy(products, el => {return el.qty*el.price});

        return (
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align="center">Qty</StyledTableCell>
                            <StyledTableCell align="center">Product</StyledTableCell>
                            <StyledTableCell align="center">Price</StyledTableCell>
                            <StyledTableCell align="center">Sub Total</StyledTableCell>
                            <StyledTableCell align="center">Disc</StyledTableCell>
                            <StyledTableCell align="center">Total</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {
                        products.map((row, indx) => {
                            if(row.disc){
                                row.disc > 80 ? 
                                    lineDisc += row.disc : 
                                    lineDisc += (row.price*row.qty)-((row.price*row.qty)*(row.disc/100)) 
                            }

                            return (
                                <>
                                <StyledTableRow key={row.idProduct}>
                                    <StyledTableCell >{row.qty}</StyledTableCell>
                                    <StyledTableCell component="th" scope="row">
                                        {row.productName}
                                    </StyledTableCell>
                                    <StyledTableCell align="right">{priceConvert(row.price)}</StyledTableCell>
                                    <StyledTableCell align="right">{priceConvert(row.price*row.qty)}</StyledTableCell>
                                    <StyledTableCell align={row.disc && row.disc > 80 ? "right" : "center"}>{
                                        row.disc ?
                                        row.disc > 80 ?
                                                priceConvert(row.disc)
                                            :
                                                row.disc + "%"
                                        : "-"
                                    }</StyledTableCell>
                                    <StyledTableCell align="right">
                                        {row.disc ? 
                                            row.disc < 80 ? priceConvert((row.price*row.qty)-((row.price*row.qty)*(row.disc/100))) :
                                                priceConvert((row.price*row.qty)-row.disc) : 
                                            priceConvert(row.price*row.qty)}
                                    </StyledTableCell>
                                </StyledTableRow>
                                </>
                            )
                        })
                    }
                    {   
                        <>
                        <StyledTableRow backgroundColor="#e0e0e0">
                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Gross Amount</StyledTableCell>
                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                    priceConvert(grossAmount)
                                }</StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow backgroundColor="#e0e0e0">
                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Discount</StyledTableCell>
                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                    priceConvert(lineDisc)
                                }</StyledTableCell>
                        </StyledTableRow>
                        <StyledTableRow backgroundColor="#e0e0e0">
                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Net Amount</StyledTableCell>
                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                    priceConvert(grossAmount - lineDisc)
                                }</StyledTableCell>
                        </StyledTableRow>
                        </>
                    }
                    </TableBody>
                </Table>
            </TableContainer>
        );

    }

    render() {
        const { classes, data } = this.props;
        let dataDetail = data;
        // let dataDetail = {
        //     "idTransaction": "TRANS0001",
        //     "buyer": "Silo ",
        //     "description": "Test Transaction",
        //     "netAmount": 2553030.0,
        //     "createdDate": "2021-03-22",
        //     "products": [
        //         {
        //             "idProduct": "PRD0003",
        //             "productName": "Dancow",
        //             "price": 45000.0,
        //             "qty": 9
        //         },
        //         {
        //             "idProduct": "PRD0005",
        //             "productName": "Nescafe Cappucino 500gr",
        //             "price": 48500.0,
        //             "qty": 7
        //         },
        //         {
        //             "idProduct": "PRD0001",
        //             "productName": "Nestlé Koko Krunch 330 gr",
        //             "price": 39900.0,
        //             "qty": 9
        //         },
        //         {
        //             "idProduct": "PRD0007",
        //             "productName": "Nestle Batita 900gr",
        //             "price": 68000.0,
        //             "qty": 6
        //         },
        //         {
        //             "idProduct": "PRD0004",
        //             "productName": "Lactogen Lactogrow 750gr",
        //             "price": 95000.0,
        //             "qty": 8
        //         },
        //         {
        //             "idProduct": "PRD0002",
        //             "productName": "Milo",
        //             "price": 30000.0,
        //             "qty": 8
        //         },
        //         {
        //             "idProduct": "PRD0006",
        //             "productName": "Nestle pure life 330ml 24pcs",
        //             "price": 37000.0,
        //             "qty": 6
        //         }
        //     ],
        //     "promotions": [
        //         {
        //             "idPromotion": "PROMO0002",
        //             "promotionDesc": "Disc. 20% Milo dan Koko Crunch",
        //             "startDate": "2021-03-10",
        //             "endDate": "2021-03-31",
        //             "forPurchase": 100000.0,
        //             "method": "Discount",
        //             "whatCustBuy": [
        //                 {
        //                     "idProduct": "PRD0001",
        //                     "productName": "Nestlé Koko Krunch 330 gr",
        //                     "price": 39900.0,
        //                     "qty": 432
        //                 },
        //                 {
        //                     "idProduct": "PRD0002",
        //                     "productName": "Milo",
        //                     "price": 30000.0,
        //                     "qty": 441
        //                 }
        //             ],
        //             "whatCustGet": [
        //                 {
        //                     "idWcg": "c8ecbf7d-d83e-4249-b99d-8674ac82d2b7",
        //                     "freeGood": null,
        //                     "discount": 20.0,
        //                     "qty": 0
        //                 }
        //             ]
        //         },
        //         {
        //             "idPromotion": "PROMO0003",
        //             "promotionDesc": "Dancow Disc. 15%",
        //             "startDate": "2021-03-10",
        //             "endDate": "2021-04-30",
        //             "forPurchase": 50000.0,
        //             "method": "Discount",
        //             "whatCustBuy": [
        //                 {
        //                     "idProduct": "PRD0003",
        //                     "productName": "Dancow",
        //                     "price": 45000.0,
        //                     "qty": 430
        //                 }
        //             ],
        //             "whatCustGet": [
        //                 {
        //                     "idWcg": "f3734f49-9d21-4f48-a1be-d35ab8a3013b",
        //                     "freeGood": null,
        //                     "discount": 15.0,
        //                     "qty": 0
        //                 }
        //             ]
        //         },
        //         {
        //             "idPromotion": "PROMO0012",
        //             "promotionDesc": "Buy Nescafe Rp.100000 Get 1 Nestle Purelife",
        //             "startDate": "2021-03-18",
        //             "endDate": "2021-03-31",
        //             "forPurchase": 100000.0,
        //             "method": "Free Good",
        //             "whatCustBuy": [
        //                 {
        //                     "idProduct": "PRD0005",
        //                     "productName": "Nescafe Cappucino 500gr",
        //                     "price": 48500.0,
        //                     "qty": 451
        //                 }
        //             ],
        //             "whatCustGet": [
        //                 {
        //                     "idWcg": "1d71b1ac-4a50-4d42-a76a-f3d9cd527064",
        //                     "freeGood": {
        //                         "idProduct": "PRD0006",
        //                         "productName": "Nestle pure life 330ml 24pcs",
        //                         "price": 37000.0,
        //                         "qty": 487
        //                     },
        //                     "discount": 0.0,
        //                     "qty": 1
        //                 }
        //             ]
        //         }
        //     ]
        // }
        let lineDisc = 0;
        let discProduct = 0;
        let netAmount = 0;
        let totalAmount = 0;
        let promotions = dataDetail.promotions;
        let cellProduct = dataDetail.products.length;
        let dataRange = Math.ceil(dataDetail.products.length / 5);
        
        
        dataDetail.products.forEach((value, index) => {
            totalAmount += value.price*value.qty;
        });

        promotions.forEach(val => {
            if (val.method === "Discount") {
                if (val.whatCustGet[0].discount < 80) {
                    let purchase = 0;
                    val.whatCustBuy.forEach(el => {
                        let findProduct = dataDetail.products.find(elm => elm.idProduct === el.idProduct);
                        purchase += findProduct.price*findProduct.qty;
                    });
                    lineDisc += purchase*(val.whatCustGet[0].discount/100);
                }else{
                    lineDisc += val.whatCustGet[0].discount
                }
                if (val.whatCustBuy.length === 1) {
                    let index = dataDetail.products.findIndex(item => item.idProduct === val.whatCustBuy[0].idProduct);
                    dataDetail.products[index] = {
                        ...dataDetail.products[index],
                        disc : val.whatCustGet[0].discount
                    }
                }else if(val.whatCustBuy.length > 2){
                    cellProduct += 1;
                }
            }else if(val.method === "Free Good"){
                cellProduct += val.whatCustGet.length;
            }
        });
        dataDetail.products.forEach(el => {
            if(el.disc){
                el.disc >= 80 ? 
                    discProduct += el.disc :
                    discProduct += (el.disc/100) * (el.price*el.qty)
            }
        })

        netAmount = totalAmount - lineDisc;
        dataDetail = {
            ...dataDetail,
            totalAmount,
            lineDisc,
            promotions : _.orderBy(dataDetail.promotions, elm => elm.method, ['desc'])   
        }

        let pageRange = dataRange + (Math.ceil((cellProduct - dataDetail.products.length)/5))

        let dataTable = []; 
        if (cellProduct > 6) {

            for (let i = 0; i < dataRange; i++) {
                dataTable.push(
                    <Grid container className={classes.root} >
                        <Grid item container spacing={3}>
                            <Grid item xs={12} container alignItems="flex-end" justify="center" style={{borderBottom:"1px solid black"}}>
                                <Typography variant="h3" style={{fontWeight:500}}>Invoice</Typography>
                            </Grid>
                            <Grid item xs={12} container justify="flex-end">
                                {`Page ${i+1} / ${pageRange}`}
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container alignItems="flex-start">
                                    <Grid item xs={8} container spacing={1}>
                                        <Grid item xs={3}>Invoice No</Grid>
                                        <Grid item xs={1}>:</Grid>
                                        <Grid item xs={8}>{dataDetail.idTransaction}</Grid>
                                        <Grid item xs={3}>Desciption</Grid>
                                        <Grid item xs={1}>:</Grid>
                                        <Grid item xs={8}>{dataDetail.description}</Grid>
                                    </Grid>
                                    <Grid item xs={4} container spacing={1} >
                                        <Grid item xs={3}>Date</Grid>
                                        <Grid item xs={1}>:</Grid>
                                        <Grid item xs={8}>{dataDetail.createdDate}</Grid>
                                        <Grid item xs={3}>Buyer</Grid>
                                        <Grid item xs={1}>:</Grid>
                                        <Grid item xs={8}>{dataDetail.buyer}</Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                {this.drawTable(dataDetail.products.slice(i * 5, (i * 5) + 5),classes)}
                            </Grid>
                        </Grid>
                    </Grid>
                )
            }
            dataTable.push(
                <Grid container className={classes.root} >
                    <Grid item container spacing={3}>
                        <Grid item xs={12} container alignItems="flex-end" justify="center" style={{borderBottom:"1px solid black"}}>
                            <Typography variant="h3" style={{fontWeight:500}}>Invoice</Typography>
                        </Grid>
                            <Grid item xs={12} container justify="flex-end">
                                {`Page ${pageRange} / ${pageRange}`}
                            </Grid>
                        <Grid item xs={12}>
                            <Grid container alignItems="flex-start">
                                <Grid item xs={8} container spacing={1}>
                                    <Grid item xs={3}>Invoice No</Grid>
                                    <Grid item xs={1}>:</Grid>
                                    <Grid item xs={8}>{dataDetail.idTransaction}</Grid>
                                    <Grid item xs={3}>Desciption</Grid>
                                    <Grid item xs={1}>:</Grid>
                                    <Grid item xs={8}>{dataDetail.description}</Grid>
                                </Grid>
                                <Grid item xs={4} container spacing={1} >
                                    <Grid item xs={3}>Date</Grid>
                                    <Grid item xs={1}>:</Grid>
                                    <Grid item xs={8}>{dataDetail.createdDate}</Grid>
                                    <Grid item xs={3}>Buyer</Grid>
                                    <Grid item xs={1}>:</Grid>
                                    <Grid item xs={8}>{dataDetail.buyer}</Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <TableContainer component={Paper}>
                                <Table className={classes.table} aria-label="customized table">
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell align="center">Qty</StyledTableCell>
                                            <StyledTableCell align="center">Product</StyledTableCell>
                                            <StyledTableCell align="center">Price</StyledTableCell>
                                            <StyledTableCell align="center">Sub Total</StyledTableCell>
                                            <StyledTableCell align="center">Disc</StyledTableCell>
                                            <StyledTableCell align="center">Total</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <StyledTableRow>
                                            <StyledTableCell >{_.sumBy(dataDetail.products, el => (el.qty))}</StyledTableCell>
                                            <StyledTableCell component="th" scope="row">
                                                Summary of Products
                                            </StyledTableCell>
                                            <StyledTableCell align="right">-</StyledTableCell>
                                            <StyledTableCell align="right">{priceConvert(_.sumBy(dataDetail.products, el => {return el.qty*el.price}))}</StyledTableCell>
                                            <StyledTableCell align="center">{priceConvert(discProduct)}</StyledTableCell>
                                            <StyledTableCell align="right">
                                                {priceConvert(_.sumBy(dataDetail.products, el => {return el.qty*el.price}) - discProduct)}
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    {
                                        dataDetail.promotions.length > 0 &&
                                            dataDetail.promotions.map((el,indx) => {
                                                if(el.method === "Free Good"){
                                                    let freeGood = el.whatCustGet.map(elm => {
                                                        return(
                                                            <StyledTableRow key={elm.idWcg}>
                                                                <StyledTableCell >{elm.qty}</StyledTableCell>
                                                                <StyledTableCell component="th" scope="row">
                                                                    {elm.freeGood.productName}
                                                                </StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                <StyledTableCell align="center">-</StyledTableCell>
                                                                <StyledTableCell align="right">
                                                                    {priceConvert(0)}
                                                                </StyledTableCell>
                                                            </StyledTableRow>
                                                        )
                                                    })
                                                    return freeGood;
                                                }else if(el.method === "Discount" && el.whatCustBuy.length > 1){
                                                    let desc = "Buy ";
                                                    let purchase = 0;
                                                    let item = el.whatCustBuy.map(el => el.productName);
                                                    el.whatCustBuy.forEach(item => {
                                                        let findProduct = dataDetail.products.find(elm => elm.idProduct === item.idProduct);
                                                        purchase += findProduct.price*findProduct.qty;
                                                    })

                                                    desc += item.join(" & ");
                                                    desc += ` for purchase ${priceConvert(el.forPurchase)} , total purchase : ${priceConvert(purchase)}`;
                                                    return (
                                                        <StyledTableRow key={el.idPromotion}>
                                                            <StyledTableCell colSpan="4">
                                                                {desc}
                                                            </StyledTableCell>
                                                            <StyledTableCell align="center">
                                                                {el.whatCustGet[0].discount >= 80 ? 
                                                                    priceConvert(el.whatCustGet[0].discount) : 
                                                                    el.whatCustGet[0].discount+"%"
                                                                }
                                                            </StyledTableCell>
                                                            <StyledTableCell align="right">{
                                                                el.whatCustGet[0].discount >= 80 ? 
                                                                    priceConvert(0 - (purchase - el.whatCustGet[0].discount)) : 
                                                                    "-(" + priceConvert((purchase*(el.whatCustGet[0].discount/100)))+" )"
                                                            }</StyledTableCell>
                                                        </StyledTableRow>
                                                    )
                                                }
                                            })
                                    }
                                    {   
                                        <>
                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Gross Amount</StyledTableCell>
                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                    priceConvert(_.sumBy(dataDetail.products, el => {return el.qty*el.price}))
                                                }</StyledTableCell>
                                        </StyledTableRow>
                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Discount</StyledTableCell>
                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                    priceConvert(dataDetail.lineDisc)
                                                }</StyledTableCell>
                                        </StyledTableRow>
                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Net Amount</StyledTableCell>
                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                    priceConvert(dataDetail.netAmount)
                                                }</StyledTableCell>
                                        </StyledTableRow>
                                        </>
                                    }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </Grid>
                </Grid>
            )
        }else{
            dataTable.push(
                <Grid container className={classes.root} >
                    <Grid item container spacing={3}>
                        <Grid item xs={12} container alignItems="flex-end" justify="center" style={{borderBottom:"1px solid black"}}>
                            <Typography variant="h3" style={{fontWeight:500}}>Invoice</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container alignItems="flex-start">
                                <Grid item xs={8} container spacing={1}>
                                    <Grid item xs={3}>Invoice No</Grid>
                                    <Grid item xs={1}>:</Grid>
                                    <Grid item xs={8}>{dataDetail.idTransaction}</Grid>
                                    <Grid item xs={3}>Desciption</Grid>
                                    <Grid item xs={1}>:</Grid>
                                    <Grid item xs={8}>{dataDetail.description}</Grid>
                                </Grid>
                                <Grid item xs={4} container spacing={1} >
                                    <Grid item xs={3}>Date</Grid>
                                    <Grid item xs={1}>:</Grid>
                                    <Grid item xs={8}>{dataDetail.createdDate}</Grid>
                                    <Grid item xs={3}>Buyer</Grid>
                                    <Grid item xs={1}>:</Grid>
                                    <Grid item xs={8}>{dataDetail.buyer}</Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <TableContainer component={Paper}>
                                <Table className={classes.table} aria-label="customized table">
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell align="center">Qty</StyledTableCell>
                                            <StyledTableCell align="center">Product</StyledTableCell>
                                            <StyledTableCell align="center">Price</StyledTableCell>
                                            <StyledTableCell align="center">Sub Total</StyledTableCell>
                                            <StyledTableCell align="center">Disc</StyledTableCell>
                                            <StyledTableCell align="center">Total</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {
                                        dataDetail.products.map((row, indx) => {
                                            return (
                                                <>
                                                <StyledTableRow key={row.idProduct}>
                                                    <StyledTableCell >{row.qty}</StyledTableCell>
                                                    <StyledTableCell component="th" scope="row">
                                                        {row.productName}
                                                    </StyledTableCell>
                                                    <StyledTableCell align="right">{priceConvert(row.price)}</StyledTableCell>
                                                    <StyledTableCell align="right">{priceConvert(row.price*row.qty)}</StyledTableCell>
                                                    <StyledTableCell align={row.disc && row.disc > 80 ? "right" : "center"}>{
                                                        row.disc ?
                                                        row.disc > 80 ?
                                                                priceConvert(row.disc)
                                                            :
                                                                row.disc + "%"
                                                        : "-"
                                                    }</StyledTableCell>
                                                    <StyledTableCell align="right">
                                                        {row.disc ? 
                                                            row.disc < 80 ? priceConvert((row.price*row.qty)-((row.price*row.qty)*(row.disc/100))) :
                                                                priceConvert((row.price*row.qty)-row.disc) : 
                                                            priceConvert(row.price*row.qty)}
                                                    </StyledTableCell>
                                                </StyledTableRow>
                                                </>
                                            )
                                        })
                                    }
                                    {
                                        dataDetail.promotions.length > 0 &&
                                            dataDetail.promotions.map((el,indx) => {
                                                if(el.method === "Free Good"){
                                                    let freeGood = el.whatCustGet.map(elm => {
                                                        return(
                                                            <StyledTableRow key={elm.idWcg}>
                                                                <StyledTableCell >{elm.qty}</StyledTableCell>
                                                                <StyledTableCell component="th" scope="row">
                                                                    {elm.freeGood.productName}
                                                                </StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                                <StyledTableCell align="center">-</StyledTableCell>
                                                                <StyledTableCell align="right">{priceConvert(0)}</StyledTableCell>
                                                            </StyledTableRow>
                                                        )
                                                    })
                                                    return freeGood;
                                                }else if(el.method === "Discount" && el.whatCustBuy.length > 1){
                                                    let desc = "Buy ";
                                                    let purchase = 0;
                                                    let item = el.whatCustBuy.map(el => el.productName);
                                                    el.whatCustBuy.forEach(item => {
                                                        let findProduct = dataDetail.products.find(elm => elm.idProduct === item.idProduct);
                                                        purchase += findProduct.price*findProduct.qty;
                                                    })

                                                    desc += item.join(" & ");
                                                    desc += ` for purchase ${priceConvert(el.forPurchase)} , total purchase : ${priceConvert(purchase)}`;
                                                    return (
                                                        <StyledTableRow key={el.idPromotion}>
                                                            <StyledTableCell colSpan="4">
                                                                {desc}
                                                            </StyledTableCell>
                                                            <StyledTableCell align="center">
                                                                {el.whatCustGet[0].discount >= 80 ? 
                                                                    priceConvert(el.whatCustGet[0].discount) : 
                                                                    el.whatCustGet[0].discount+"%"
                                                                }
                                                            </StyledTableCell>
                                                            <StyledTableCell align="right">{
                                                                el.whatCustGet[0].discount >= 80 ? 
                                                                    priceConvert(0 - (purchase - el.whatCustGet[0].discount)) : 
                                                                    "-(" + priceConvert((purchase*(el.whatCustGet[0].discount/100)))+" )"
                                                            }</StyledTableCell>
                                                        </StyledTableRow>
                                                    )
                                                }
                                            })
                                    }
                                    {   
                                        <>
                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Gross Amount</StyledTableCell>
                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                    priceConvert(_.sumBy(dataDetail.products, el => {return el.qty*el.price}))
                                                }</StyledTableCell>
                                        </StyledTableRow>
                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Total Discount</StyledTableCell>
                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                    priceConvert(dataDetail.lineDisc)
                                                }</StyledTableCell>
                                        </StyledTableRow>
                                        <StyledTableRow backgroundColor="#e0e0e0">
                                                <StyledTableCell colSpan="5" align="right" style={{fontWeight:"bold"}}>Net Amount</StyledTableCell>
                                                <StyledTableCell align="right" style={{fontWeight:"bold"}}>{
                                                    priceConvert(dataDetail.netAmount)
                                                }</StyledTableCell>
                                        </StyledTableRow>
                                        </>
                                    }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </Grid>
                </Grid>
            )
        }

        return (
            <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="flex-start"
            >
                <Grid container item xs={12}>
                    {dataTable.map((el, indx) => {
                        return (
                        <Box 
                            key={indx}
                            style={{
                                height: "750px",
                                width: "1120px",
                                marginLeft: "auto",
                                marginRight: "auto",
                                // marginBottom: "10px",
                            }}
                        >
                            {el}
                        </Box>
                        )
                    })}
                </Grid>
            </Grid>
        );
    }
}
 
export default withStyles(styles, {withTheme:true})(PrintTransaction);