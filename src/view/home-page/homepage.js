import React, { Component } from 'react'
import DateFnsUtils from '@date-io/date-fns';
import { withStyles, createMuiTheme } from "@material-ui/core/styles"
import { Grid, Typography, 
    ThemeProvider, Paper } 
from "@material-ui/core";
import {connect} from "react-redux";
import moment from "moment";
import _ from "lodash";
import { Doughnut, Line } from "@reactchartjs/react-chart.js";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import Action from "../../action";
import getRandomColor from "../../util/getRandomColor";
import { blue, red } from "@material-ui/core/colors"


const styles = theme => ({
    root : {
        padding : theme.spacing(4)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    mt2 :{
        margin: "2px"
    },
    mAuto : {
        margin :"auto"
    },
    rootPaper: {
        display: 'flex',
        flexWrap: 'wrap',
        '& > *': {
          width: "100%",  
          height: "57vh",
        },
    },
});

const custTheme = createMuiTheme({
    overrides: {
        MuiPickersToolbar: {
          toolbar: {
            backgroundColor: "#c70039",
          },
        },
        MuiPickersDay: {
          day: {
            color: "#000",
          },
          daySelected: {
            backgroundColor: "#c70039",
          },
          dayDisabled: {
            color: red["100"],
          },
          current: {
            color: red["900"],
          },
        },
        MuiPickersModal: {
          dialogAction: {
            color: red["400"],
          },
        },
    },
});

class Homepage extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            dataPromotion : {},
            dataTransaction : {},
            selectedDate : new Date(),
        }
        this.handleDateChange = this.handleDateChange.bind(this);
        this.getData = this.getData.bind(this);
    }

    componentDidMount(){
        Promise.all([
            fetch(`http://localhost:8080/api/sales/promotion/report`,
            {
                method : "GET",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization" : "Bearer " + this.props.token
                }
            }),
            fetch(`http://localhost:8080/api/sales/transaction/report?date=${moment().format("YYYY-MM-DD")}`,
            {
                method : "GET",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization" : "Bearer " + this.props.token
                }
            })
        ])
        .then(([resp, resp2]) => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    throw new Error(`${text.message}`);
                })
            }
            if (!resp2.ok) {
                if (resp2.status === 403){
                    return resp2.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp2.json().then(text => {
                    throw new Error(`${text.message}`);
                })
            }
            // let output = [resp.json(), resp2.json()];
            return Promise.all([resp.json(), resp2.json()])
        })
        .then(([json, json2]) => {

            const dataDougnat = {
                labels: json.map(el => el.name),
                datasets: [
                  {
                    data: json.map(el => el.value),
                    backgroundColor: [blue[500], red[500]],
                    borderWidth: 1,
                  },
                ],
            }
            const dataLine = {
                labels: json2.map(el => moment(el.date).format("MMMM DD")),
                datasets: [
                  {
                    label: 'Transaction',
                    data: json2.map(el => el.count),
                    fill: false,
                    backgroundColor: '#c70039',
                    borderColor: '#f9054b',
                  },
                ],
            }

            this.setState({
                dataPromotion : dataDougnat,
                dataTransaction : dataLine
            })
        })
        .catch(err => {
            alert(err);
        })
    }

    handleDateChange(value){
        this.setState({
            selectedDate :value
        }, () => {
            this.getData();
        })
    }

    getData(){
        fetch(`http://localhost:8080/api/sales/transaction/report?date=${moment(this.state.selectedDate).format("YYYY-MM-DD")}`,
        {
            method : "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization" : "Bearer " + this.props.token
            }
        })
        .then((resp) => {
            if (!resp.ok) {
                if (resp.status === 403){
                    return resp.json().then(text => {
                        alert(text.message);
                        this.props.logout();
                        this.props.history.push("/login");
                    })
                }
                return resp.json().then(text => {
                    throw new Error(`${text.message}`);
                })
            }
            // let output = [resp.json(), resp2.json()];
            return resp.json()
        })
        .then((json) => {
            const dataLine = {
                labels: json.map(el => moment(el.date).format("DD")),
                datasets: [
                  {
                    label: 'Transaction',
                    data: json.map(el => el.count),
                    fill: false,
                    backgroundColor: '#c70039',
                    borderColor: '#f9054b',
                  },
                ],
            }
            this.setState({
                dataTransaction : dataLine
            })
        })
        .catch(err => {
            alert(err);
        })
    }

    render() { 
        const { classes } = this.props;
        const { dataTransaction , dataPromotion : chartData,
            selectedDate} = this.state;
        let maxValue = _.maxBy(dataTransaction, 'count');
        const options = {
            scales: {
              yAxes: [
                {
                  ticks: {
                    beginAtZero: true,
                  },
                },
              ],
            },
        }
        return (
            <Grid container spacing={3} className={classes.root}>
                <Grid item xs={12}>
                    <Typography align="center" style={{fontWeight:"500"}} variant="h4" component="h5">
                        Selamat datang di ND6 sales Promotion !!
                    </Typography> 
                </Grid>
                <Grid item xs={12}>
                    {/* <div className={classes.rootPaper}>
                        <Paper elevation={3} style={{padding:"3vh"}}>

                            
                        </Paper>
                    </div> */}
                    <Paper elevation={3} style={{padding:"3vh"}}>
                        <Grid container spacing={3} alignItems="center">
                            <Grid item xs={5}>
                                <Typography variant="h5" align="center" style={{fontWeight:"bold"}}>Report Promotion</Typography>
                            </Grid>
                            <Grid item xs={7}>
                                <Typography variant="h5" align="center" style={{fontWeight:"bold"}}>
                                    Monthly Report Transaction
                                </Typography>
                                <div style={{float:"right"}}>
                                    <ThemeProvider theme={custTheme}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <DatePicker
                                                views={["year", "month"]}
                                                label="Year and Month"
                                                value={selectedDate}
                                                maxDate={new Date()}
                                                onChange={this.handleDateChange}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </ThemeProvider>
                                </div>
                                
                            </Grid>
                            <Grid item xs={5}>
                                { 
                                    Object.keys(chartData).length > 0 &&
                                        <Doughnut data={chartData}/>
                                }
                            </Grid>
                            <Grid item xs={7}>
                                {
                                    Object.keys(dataTransaction).length > 0 && 
                                    <Line data={dataTransaction} options={options} />
                                }
                            </Grid>
                        </Grid>
                    </Paper>
                    {/* <Grid item xs={12}>
                    </Grid>
                    <Grid item xs={8}>
                        <Paper elevation={3} style={{padding:"3vh"}}>
                            {
                                Object.keys(dataTransaction).length > 0 && 
                                <Line data={dataTransaction} options={options} />
                            }
                        </Paper>
                    </Grid> */}
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return{
        isLogin : state.AuthReducer.statusLogin,
        validCaptcha : state.RecaptReducer.validate,
        token : state.AuthReducer.token
    }
}

const mapDispatchToProps = dispatch => {
    return{
        doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin}),
        setTokenRecapt : (token) => dispatch({ type : Action.SET_RECAPT_TOKEN, payload : token}),
        validRecapt : () => dispatch({ type : Action.VALID_RECAPT}),
        logout : () => dispatch({ type : Action.LOGOUT})
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, {withTheme:true})(Homepage));
