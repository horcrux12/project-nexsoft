import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { Login } from "./view";
import { connect } from "react-redux";

import Main from "./template/main/main";
// import Page404 from "./view/404page";
import PrintTransaction from "./view/pdf/transaction-pdf";
import Action from "./action";
// import ScrollableTabsButtonForce from "./cobaTabs";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      recaptToken : null
    }
    this.setRecaptToken = this.setRecaptToken.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState){
    if (nextState.recaptToken !== this.state.recaptToken) {
      return false;
    }else{
      return true;
    }
  }

  setRecaptToken(value){
    this.setState({
      recaptToken : value
    })
  }

  render() { 
    return (  
      <>
        <Router>
          <Switch>
            <Route path="/admin" component={props => <Main {...props}/>} />
            <Route path="/login" component={props => <Login {...props} setRecaptToken={this.setRecaptToken}/>} />
            <Route path="/print-invoice" component={props => <PrintTransaction {...props}/>}/>
            <Redirect from="/" to="/admin/home"/>
            
          </Switch>
        </Router>
      </>
    );
  }
}

const mapStateToProps = state => {
  return{
      isLogin : state.AuthReducer.statusLogin
  }
}

const mapDispatchToProps = dispatch => {
  return{
      doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);