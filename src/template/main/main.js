import React, { Component } from 'react';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

// Local Import

import Action from "../../action";
import Body from "../../components/body/body";
import Sidebar from "../../components/sidebar/sidebar";
import Navbar from "../../components/navbar/navbar";

import Homepage from "../../view/home-page/homepage";
import TablePromotion from "../../view/promotion/table";
import FormPromotion from "../../view/promotion/form";
import TableTransaction from "../../view/transaction/table";
import FormTransaction from "../../view/transaction/form";

import "./style.css";

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            sideBarColl : false
        }
        this.handleCollapse = this.handleCollapse.bind(this);
    }

    handleCollapse(){
        this.setState({
            sideBarColl : !this.state.sideBarColl
        })
    }

    render() { 
        const { match, isLogin } = this.props;
        // console.log(isLogin);
        if (!isLogin) {
           return (<Redirect to="/login"/>) 
        }
        let routes = [
            {
                link : match.path + "/home",
                location : match.path + "/home",
                comp : Homepage,
                title : "Home Page",
                root : "Home"
            },
            {
                link : match.path + "/promotion",
                location : match.path + "/promotion",
                comp : TablePromotion,
                title : "Promotion Table ",
                root : "Promotion"
            },
            {
                link : match.path + "/add-promotion",
                location : match.path + "/add-promotion",
                comp : FormPromotion,
                title : "Add Promotion",
                root : "Promotion"
            },
            {
                link : match.path + "/edit-promotion/:id",
                location : match.path + "/edit-promotion",
                comp : FormPromotion,
                title : "Edit Promotion",
                root : "Promotion"
            },
            {
                link : match.path + "/transaction",
                location : match.path + "/transaction",
                comp : TableTransaction,
                title : "Transaction Table",
                root : "Transaction"
            },
            {
                link : match.path + "/add-transaction",
                location : match.path + "/add-transaction",
                comp : FormTransaction,
                title : "Add Transaction",
                root : "Transaction"
            },
            {
                link : match.path + "/edit-transaction/:id",
                location : match.path + "/edit-transaction",
                comp : FormTransaction,
                title : "Edit Transaction",
                root : "Transaction"
            },

        ]
        // console.log(this.props);
        return (  
            <div className="main">
                <div className="sidebar-wrapper">
                    <Sidebar {...this.props} routes={routes} sideBarColl={this.state.sideBarColl} handleCollapse={this.handleCollapse}/>
                </div>
                <div className={this.state.sideBarColl ? "main-content main-toggled" : "main-content main-untoggled"}>
                    <div className="navbar-wrapper">
                        <Navbar {...this.props} routes={routes}/>
                    </div>
                    <div className="content">
                        <Body {...this.props} routes={routes}/>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return{
        isLogin : state.AuthReducer.statusLogin
    }
}

const mapDispatchToProps = dispatch => {
    return{
        doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin})
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(Main);