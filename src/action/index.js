const action = {
    LOGIN_SUCCCESS : "LOGIN_SUCCCESS",
    LOGOUT : "LOGOUT",
    SET_RECAPT_TOKEN : "SET_RECAPT_TOKEN",
    VALID_RECAPT : "VALID_RECAPT"
}

export default action;