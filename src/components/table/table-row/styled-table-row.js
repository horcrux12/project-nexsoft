import { TableRow } from "@material-ui/core";
import { withStyles, createStyles } from "@material-ui/core/styles";

const StyledTableRow = withStyles((theme) =>
  createStyles({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      }
    },
  }),
)(TableRow);

export default StyledTableRow;