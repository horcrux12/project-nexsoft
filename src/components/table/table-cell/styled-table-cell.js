import { TableCell } from "@material-ui/core";
import { withStyles, createStyles } from "@material-ui/core/styles";

const StyledTableCell = withStyles((theme) =>
  createStyles({
    head: {
      backgroundColor: "#1e212d",
      color: theme.palette.common.white,
    },
    th : {
      backgroundColor: "#1e212d",
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);

export default StyledTableCell;