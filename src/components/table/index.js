import StyledTableCell from "./table-cell/styled-table-cell";
import StyledTableRow from "./table-row/styled-table-row";

export { StyledTableCell, StyledTableRow };