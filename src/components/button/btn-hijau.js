import { Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

const ButtonHijau = withStyles((theme) => ({
    root: {
      color: "#FFF",
      backgroundColor: green[500],
      '&:hover': {
        backgroundColor: green[700],
      },
    },
}))(Button);

export default ButtonHijau;