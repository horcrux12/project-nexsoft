import { Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { blue } from "@material-ui/core/colors";

const ButtonBiru = withStyles((theme) => ({
    root: {
      color: "#FFF",
      backgroundColor: blue[700],
      '&:hover': {
        backgroundColor: blue[900],
      },
    },
}))(Button);

export default ButtonBiru;