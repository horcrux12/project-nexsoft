import { Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { grey } from "@material-ui/core/colors";

const ButtonAbu = withStyles((theme) => ({
    root: {
      color: "#FFF",
      backgroundColor: grey[600],
      '&:hover': {
        backgroundColor: grey[700],
      },
    },
}))(Button);

export default ButtonAbu;