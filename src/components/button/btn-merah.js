import { Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";

const ButtonMerah = withStyles((theme) => ({
    root: {
      color: "#FFF",
      backgroundColor: red[700],
      '&:hover': {
        backgroundColor: red[900],
      },
    },
}))(Button);

export default ButtonMerah;