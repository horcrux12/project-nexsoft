import React, { Component } from 'react';
import DehazeIcon from '@material-ui/icons/Dehaze';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import CloseIcon from '@material-ui/icons/Close';
import ListAltIcon from '@material-ui/icons/ListAlt';

import {Grid, IconButton, ListSubheader,
List , ListItem, ListItemIcon, ListItemText,
Typography
} from "@material-ui/core";
import { withStyles} from "@material-ui/core/styles"
import HomeIcon from '@material-ui/icons/Home';
import logoND6 from "../../assets/image/nd6antelope.png";

const styles = theme => ({
    whiteColor : {
        color : "#fff"
    },
    "&.Mui-selected": {
        backgroundColor: "red"
    },
    ListSubheader : {
        fontSize:'.9em',//Insert your required size
        fontWeight : "500",
        color : "#fff"
    },
    listItemText:{
        fontSize:'1.2em',//Insert your required size
        fontWeight : "500",
        color : "#fff"
    },
    logo : {
        height : "7em"
    },
    imageBotom : {
        position : "fixed",
        bottom : "3vh",
        left : "33px"
    },
    hidden : {
        display : "none"
    }
})

const ListItems = withStyles({
    root: {
      "&$selected": {
        backgroundColor: "rgb(207 207 207 / 13%)",
      },
      "&$selected&:hover": {
        backgroundColor: "rgb(207 207 207 / 13%)",
      },
      "&:hover": {
        backgroundColor: "rgb(207 207 207 / 13%)",
      }
    },
    selected: {}
})(ListItem);


class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            selectedItem : 0
        }
        this.handleSelectedItem = this.handleSelectedItem.bind(this)
    }

    handleSelectedItem (index, path) {
        this.setState({
            selectedItem : index
        }, () => {
            this.props.history.push(path);
        })
    }

    render() { 
        const { classes, routes, location } = this.props;
        // console.log(this.props);
        return (
            <div className={this.props.sideBarColl ? "toggled-sidebar" : "sidebar"}>
                <div className="header">
                    <Grid container spacing={1}>
                        <Grid className={this.props.sideBarColl ? "hidden" : "judul"} item xs={9}>
                            <Typography variant="h4" style={{fontWeight:"bold", fontSize: "1.5rem"}}>ND6 Apps</Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <IconButton className={classes.whiteColor} onClick={this.props.handleCollapse} aria-label="Collapse sidebar">
                                {this.props.sideBarColl ? <DehazeIcon fontSize="default"/> : <CloseIcon fontSize="default"/>}
                            </IconButton>
                        </Grid>
                    </Grid>
                </div>
                <div className="menu">
                    <List 
                        component="nav"
                        aria-label="nested-list-subheader"
                        classes={{ selected: classes.selected }}
                        subheader={
                            !this.props.sideBarColl &&
                            <ListSubheader className={classes.ListSubheader} component="div" id="nested-list-subheader">
                                Menu Promotion Sales
                            </ListSubheader>
                        }
                    >
                        <ListItems
                            button
                            selected={this.state.selectedItem === 0}
                            alignItems="center"
                            onClick={()=>{this.handleSelectedItem(0, "/admin/home")}}
                        >
                            <ListItemIcon className={classes.whiteColor}>
                                <HomeIcon fontSize="default"/>
                            </ListItemIcon>
                            <ListItemText 
                                classes={{primary:classes.listItemText}}
                                primary="Home" />
                        </ListItems>
                        <ListItems 
                            button
                            selected={this.state.selectedItem === 1}
                            alignItems="center"
                            onClick={()=>{this.handleSelectedItem(1, "/admin/promotion")}}
                        >
                            <ListItemIcon className={classes.whiteColor}>
                                <ListAltIcon fontSize="default"/>
                            </ListItemIcon>
                            <ListItemText classes={{primary:classes.listItemText}} primary="Promotion" />
                        </ListItems>
                        <ListItems 
                            button
                            selected={this.state.selectedItem === 2}
                            alignItems="center"
                            onClick={()=>{this.handleSelectedItem(2, "/admin/transaction")}}
                        >
                            <ListItemIcon className={classes.whiteColor}>
                                <ShoppingCartIcon fontSize="default"/>
                            </ListItemIcon>
                            <ListItemText classes={{primary:classes.listItemText}} primary="Transaction" />
                        </ListItems>
                    </List>
                </div>
                <div className={this.props.sideBarColl ? classes.hidden : classes.imageBotom}>
                    <img className={classes.logo} src={logoND6} alt="nd6 logo" />
                </div>
            </div>
        );
    }
}
 
export default withStyles(styles, {withTheme :  true})(Sidebar);