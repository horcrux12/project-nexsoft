import React, { Component } from 'react';
import { IconButton, Typography,
    Menu, MenuItem, Fade,
    ListItemIcon, ListItemText
} from "@material-ui/core";
import {AiOutlineLogout} from "react-icons/ai";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { Grid } from "@material-ui/core";
import { connect } from "react-redux";

import Action from "../../action";

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            anchorEl : null,
            openMenu : false
        }
        this.logoutHandle = this.logoutHandle.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleClose = this.handleClose.bind(this)
    }

    logoutHandle(){
        if (window.confirm("Apakah anda yakin ingin keluar dari aplikasi ??")) {
            this.props.history.push("/login")
        }
    }

    handleClick(event){
        this.setState({
            anchorEl : event.currentTarget,
            openMenu : Boolean(event.currentTarget)
        })
    };
    
    handleClose(target){
        this.setState({
            anchorEl : null,
            openMenu : Boolean(null)
        }, ()=>{
            if (target === "logout") {
                this.props.doLogout();
                this.props.history.push("/login")
            }
        })
    };

    render() { 
        const { location, routes } = this.props;
        const { openMenu, anchorEl } = this.state;
        return (  
            <div className="navbar">
                <Grid container spacing={3} alignItems="center">
                    <Grid item xs={11}>
                        <Typography variant="h4" style={{ fontWeight:"bold", fontSize: "1.5rem", color:"#fff", marginLeft:"2vh"}}>
                            {
                                routes.map(el => location.pathname === el.location && el.title)
                            }
                        </Typography>
                    </Grid>
                    <Grid item xs={1} className="btn-logout">
                        <Typography variant="h6">Sales</Typography>
                        <IconButton 
                            style={{color:"#fff", fontWeight:600}}
                            onClick={this.handleClick}
                        >
                            <AccountCircleIcon fontSize="default"/>
                        </IconButton>
                        <Menu
                            id="fade-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={openMenu}
                            onClose={this.handleClose}
                            TransitionComponent={Fade}
                        >
                            <MenuItem onClick={
                                    () => {
                                        if(window.confirm("Are you sure to logout ?")){
                                            this.handleClose("logout")
                                        }else{
                                            this.setState({
                                                anchorEl : null,
                                                openMenu : Boolean(null)
                                            })
                                        }
                                    }
                                }>
                                <ListItemIcon style={{minWidth: '20px'}}>
                                    <AiOutlineLogout />
                                </ListItemIcon>
                                <ListItemText primary="Logout" alignItems="center"/>
                            </MenuItem>
                        </Menu>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return{
        isLogin : state.AuthReducer.statusLogin
    }
}

const mapDispatchToProps = dispatch => {
    return{
        doLogout : () => dispatch({ type : Action.LOGOUT})
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Navbar);