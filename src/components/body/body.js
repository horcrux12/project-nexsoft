import React, { Component } from 'react';
import { Grid } from "@material-ui/core";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import Action from "../../action";
import Page404 from "../../view/404page";

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(){
        alert("ini")
    }

    render() { 
        // const { match } = this.props
        return (  
            <div className="body">
                <Grid container>                                                                                
                    <Grid item xs={12}>
                        <Switch>
                            {
                                this.props.routes.map((el, idx) => {
                                    return <Route key={idx} path={el.link} exact component={props => <el.comp {...props}/>}/>
                                })
                            }
                            <Redirect from="/" to="/admin/home"/>
                        </Switch>
                    </Grid>
                </Grid>                
            </div>
        );
    }
}
const mapStateToProps = state => {
    return{
        isLogin : state.AuthReducer.statusLogin
    }
}

const mapDispatchToProps = dispatch => {
    return{
        doLogin : (dataLogin) => dispatch({ type : Action.LOGIN_SUCCCESS, payload : dataLogin})
    }
}
 
export default connect(mapStateToProps,mapDispatchToProps)(Body);